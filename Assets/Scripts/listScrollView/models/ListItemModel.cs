﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace com.bitvillain.listScroll
{
    public class ListItemModel
    {
        public int ID;
        public GameObject GameObject;
        public List<string> Tags;
        public int ParentID = -1;
        public List<int> ChildIDs = new List<int>();
        internal DateTime Timestamp;
        internal bool IsNew = false;
        internal bool IsCollapsed = false;

        public ListItemModel(
            int id,
            GameObject gameObject,
            List<string> tags,
            int parentID,
            List<int> childrenIDs,
            DateTime timestamp,
            bool isNew = false
        )
        {
            ID = id;
            GameObject = gameObject;
            Tags = tags;
            ParentID = parentID;
            ChildIDs = childrenIDs;
            Timestamp = timestamp;
            IsNew = isNew;
        }
    }
}