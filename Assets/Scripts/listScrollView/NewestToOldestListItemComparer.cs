﻿using System.Collections.Generic;

namespace com.bitvillain.listScroll
{
    public class NewestToOldestListItemComparer : IComparer<ListItemModel>
    {
        public int Compare(ListItemModel b, ListItemModel a)
        {
            return b.Timestamp.CompareTo(a.Timestamp);
        }
    }
}