﻿using System.Collections.Generic;

namespace com.bitvillain.listScroll
{
    public class AlphabeticListItemComparer : IComparer<ListItemModel>
    {
        public int Compare(ListItemModel a, ListItemModel b)
        {
            return
                (b.Tags.Count > 0 && a.Tags.Count > 0) ?
                    string.Compare(b.Tags[0], a.Tags[0], true) :
                        (b.Tags.Count == 0 && a.Tags.Count == 0) ? 0 :
                            (b.Tags.Count == 0) ? -1 : 1;
        }
    }
}