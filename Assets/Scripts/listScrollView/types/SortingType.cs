﻿namespace com.bitvillain.listScroll
{
    public enum SortingType
    {
        None = 0,
        Alphabetical = 1,
        NewestToOldest = 2
    }
}