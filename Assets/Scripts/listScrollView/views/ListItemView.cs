﻿using System;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;

namespace com.bitvillain.listScroll
{
    public class ListItemView : View    
    {
        [SerializeField]
        Button button;

        [SerializeField]
        Image newImage;

        [SerializeField]
        Image selectedImage;

        [SerializeField]
        Image deselectedImage;

        public event Action Clicked;

        //TODO hover state?

        override protected void Awake()
        {
            base.Awake();
            SetNew(false);
            SetSelected(false);
        }

        override protected void OnEnable()
        {
            base.OnEnable();
            button.onClick.AddListener(OnClicked);
        }

        override protected void OnDisable()
        {
            base.OnDisable();
            button.onClick.RemoveListener(OnClicked);
        }

        private void OnClicked()
        {
            if(Clicked != null)
                Clicked();
        }

        public void SetNew(bool value)
        {
            if(newImage != null)
                newImage.enabled = value;
        }

        public void SetSelected(bool value)
        {
            if(deselectedImage != null)
                deselectedImage.enabled = !value;

            if(selectedImage != null)
                selectedImage.enabled = value;
        }
    }
}