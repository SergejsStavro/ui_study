﻿using UnityEngine;
using strange.extensions.mediation.impl;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

namespace com.bitvillain.listScroll
{
    public class ListScrollView : View
    {

        [SerializeField]
        InputField searchField;


        [SerializeField]
        Button sortButton;


        [SerializeField]
        Text sortButtonText;

        [SerializeField]
        bool showSubcategories = true;


        [SerializeField]
        Transform content;

        Dictionary<int, ListItemView> entryViews = new Dictionary<int, ListItemView>();
        Dictionary<int, CollapsableListItemView> collapsableEntryViews = new Dictionary<int, CollapsableListItemView>();
        Dictionary<int, Action> entryCallbacks = new Dictionary<int, Action>();
        Dictionary<int, ListItemModel> entryModels = new Dictionary<int, ListItemModel>();

        public Action<int> EntryButtonClicked;

        List<ListItemModel> roots = new List<ListItemModel>();
        Dictionary<int, ListItemModel> branches = new Dictionary<int, ListItemModel>();
        
        Stack<ListItemModel> sortingStack = new Stack<ListItemModel>();

        List<int> entryOrder = new List<int>();

        Dictionary<SortingType, IComparer<ListItemModel>> sortingComparersBySortingType;
        IComparer<ListItemModel> currentSortingComparer;
        int currentSortingTypeIndex;

        List<SortingType> sortingTypeOrder;

        override protected void Awake()
        {
            base.Awake();
            sortingComparersBySortingType = new Dictionary<SortingType, IComparer<ListItemModel>>()
            {
                {SortingType.Alphabetical, new AlphabeticListItemComparer()},
                {SortingType.NewestToOldest, new NewestToOldestListItemComparer()}
            };

            sortingTypeOrder = new List<SortingType>(){
                SortingType.Alphabetical,
                SortingType.NewestToOldest,
            };

            currentSortingTypeIndex = 0;

            currentSortingComparer = sortingComparersBySortingType[sortingTypeOrder[currentSortingTypeIndex]];

        }

        override protected void OnEnable()
        {
            base.OnEnable();
            searchField.onEndEdit.AddListener(HandleSearchEndEdit);

            sortButton.onClick.AddListener(HandleSortClicked);
        }

        override protected void OnDisable()
        {
            base.OnDisable();
            searchField.onEndEdit.RemoveListener(HandleSearchEndEdit);

            sortButton.onClick.RemoveListener(HandleSortClicked);

            var entryIDs = new List<int>(entryViews.Keys);
            for (int i = 0; i < entryIDs.Count; i++)
                entryViews[entryIDs[i]].Clicked -= entryCallbacks[entryIDs[i]];
        }

        private void HandleSearchEndEdit(string searchTerm)
        {
            bool containsTerm = false;
            foreach (var item in entryModels)
            {
                containsTerm = ItemTagsContainSearchTerm(item.Value.Tags, searchTerm);

                item.Value.GameObject.SetActive(containsTerm);
            }
        }

        private bool ItemTagsContainSearchTerm(List<string> tags, string searchTerm)
        {
            bool result = searchTerm == string.Empty; //empty string auto approves
            for (int i = 0; i < tags.Count && !result; i++)
                if (tags[i].Contains(searchTerm.ToLower()))
                    result = true;

            return result;
        }

        private void HandleSortClicked()
        {
            currentSortingTypeIndex++;
            if (currentSortingTypeIndex >= sortingTypeOrder.Count)
                currentSortingTypeIndex = 0;

            SortEntries();
        }

        public void SortEntries()
        {
            var sortingType = sortingTypeOrder[currentSortingTypeIndex];
            currentSortingComparer = sortingComparersBySortingType[sortingType];

            sortButtonText.text = sortingType.ToString();

            LoadSortingStack();

            for (int i = 0; i < entryOrder.Count; i++)
                if(entryModels.ContainsKey(entryOrder[i]))
                    entryModels[entryOrder[i]].GameObject.transform.SetSiblingIndex(i);
        }

        void LoadSortingStack()
        {
            roots.Sort(currentSortingComparer);

            sortingStack.Clear();

            foreach (ListItemModel root in roots)
                sortingStack.Push(root);

            ProcessSortingStack();
        }

        void ProcessSortingStack()
        {
            if (sortingStack.Count > 0)
            {
                ListItemModel entry = sortingStack.Pop();
                entryOrder.Add(entry.ID);

                if (entry.ChildIDs.Count > 0)
                {
                    List<ListItemModel> children = new List<ListItemModel>();

                    for (int i = 0; i < entry.ChildIDs.Count; i++)
                        if (entryModels.ContainsKey(entry.ChildIDs[i]))
                            children.Add(entryModels[entry.ChildIDs[i]]);

                    children.Sort(currentSortingComparer);

                    foreach (ListItemModel child in children)
                        sortingStack.Push(child);
                }

                ProcessSortingStack();
            }
        }

        internal void AddEntries(List<ListItemModel> entries)
        {
            foreach (var entry in entries)
                AddEntry(entry);

            SortEntries();
        }

        public void AddEntry(ListItemModel entry)
        {
            entryModels.Add(entry.ID, entry);

            AddEntry(entry.ID, entry.GameObject);

            if (entry.ParentID == -1)
                roots.Add(entry);
            else if (entry.ChildIDs.Count != 0)
                branches.Add(entry.ID, entry);

            entryViews[entry.ID].SetNew(entry.IsNew);
        }

        internal void RemoveEntry(int entryID)
        {
            entryOrder.Remove(entryID);

            if(entryModels.ContainsKey(entryID))
            {
                var entryRecord = entryModels[entryID];
                entryModels.Remove(entryID);

                if(roots.Contains(entryRecord))
                    roots.Remove(entryRecord);

                if(branches.ContainsKey(entryID))
                    branches.Remove(entryID);
            }

            if (entryViews.ContainsKey(entryID))
            {
                var entryView = entryViews[entryID];
                Action entryAction = entryCallbacks[entryID];
                entryView.Clicked -= entryAction;

                entryViews.Remove(entryID);
                entryCallbacks.Remove(entryID);

                Destroy(entryView.gameObject);
            }

            if (collapsableEntryViews.ContainsKey(entryID))
                collapsableEntryViews.Remove(entryID);
        }

        //TODO deprecated
        internal void AddEntires(Dictionary<int, GameObject> entries)
        {
            foreach (var entryKVP in entries)
                AddEntry(entryKVP.Key, entryKVP.Value);
        }

        void AddEntry(int entryID, GameObject entry)
        {
            entry.transform.SetParent(content);
            entry.transform.localScale = Vector3.one;
            entry.transform.localPosition = Vector2.zero;

            var entryView = entry.GetComponent<ListItemView>();
            Action entryAction = () => OnEntryButtonClicked(entryID);
            entryView.Clicked += entryAction;

            entryCallbacks.Add(entryID, entryAction);
            entryViews.Add(entryID, entryView);

            var collapsableEntryView = 
                entry.GetComponent<CollapsableListItemView>();

            if(collapsableEntryView != null)
                collapsableEntryViews.Add(entryID, collapsableEntryView);
        }

        private void OnEntryButtonClicked(int entryID)
        {
            if (EntryButtonClicked != null)
                EntryButtonClicked(entryID);
        }

        public void DeselectEntry(int entryID)
        {
            if (entryViews.ContainsKey(entryID))
                entryViews[entryID].SetSelected(false);
        }

        public void SelectEntry(int entryID)
        {
            if (entryViews.ContainsKey(entryID))
                entryViews[entryID].SetSelected(true);
        }

        internal void MarkEntriesNew(List<int> entryIDs)
        {
            for (int i = 0; i < entryIDs.Count; i++)
                MarkEntryNew(entryIDs[i]);
        }

        internal void MarkEntryNew(int entryID)
        {
            if (entryModels.ContainsKey(entryID))
                entryModels[entryID].IsNew = true;

            if (entryViews.ContainsKey(entryID))
                entryViews[entryID].SetNew(true);
        }

        internal void MarkEntryOld(int entryID)
        {
            if (entryModels.ContainsKey(entryID))
                entryModels[entryID].IsNew = false;

            if (entryViews.ContainsKey(entryID))
                entryViews[entryID].SetNew(false);
        }

        public void ToggleCollapseEntry(int entryID)
        {
            if (entryModels.ContainsKey(entryID))
            {
                entryModels[entryID].IsCollapsed = !entryModels[entryID].IsCollapsed;
                SetChildrenVisibility(entryID, !entryModels[entryID].IsCollapsed);

                if (collapsableEntryViews.ContainsKey(entryID))
                    collapsableEntryViews[entryID].SetCollapsed(
                        entryModels[entryID].IsCollapsed
                    );
            }
        }

        public void CollapseEntry(int entryID)
        {
            if (entryModels.ContainsKey(entryID))
            {
                entryModels[entryID].IsCollapsed = true;
                SetChildrenVisibility(entryID, false);

                if (collapsableEntryViews.ContainsKey(entryID))
                    collapsableEntryViews[entryID].SetCollapsed(true);
            }
        }

        public void ExpandEntry(int entryID)
        {
            if (entryModels.ContainsKey(entryID))
            {
                entryModels[entryID].IsCollapsed = false;
                SetChildrenVisibility(entryID, true);

                if (collapsableEntryViews.ContainsKey(entryID))
                    collapsableEntryViews[entryID].SetCollapsed(false);
            }
        }

        public void SetAllBranchVisibility(bool isVisible)
        {
            showSubcategories = isVisible;
            foreach (var entry in branches)
                if (entryViews.ContainsKey(entry.Value.ID))
                    entryViews[entry.Value.ID].gameObject.SetActive(isVisible);
        }

        void SetChildrenVisibility(int entryID, bool isVisible)
        {
            var descendantQueue = new Queue<int>(entryModels[entryID].ChildIDs);

            for (int i = 0; i < 10000 && descendantQueue.Count > 0; i++)
            {
                var descendantID = descendantQueue.Dequeue();

                if (entryViews.ContainsKey(descendantID) &&
                    (showSubcategories || !branches.ContainsKey(descendantID))
                )
                    entryViews[descendantID].gameObject.SetActive(isVisible);

                if (entryModels.ContainsKey(descendantID))
                    foreach (var childID in entryModels[descendantID].ChildIDs)
                        descendantQueue.Enqueue(childID);
            }
        }
    }
}