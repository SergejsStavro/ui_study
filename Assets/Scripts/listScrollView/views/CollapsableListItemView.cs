﻿using System;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;

namespace com.bitvillain.listScroll
{
    public class CollapsableListItemView : View    
    {
        [SerializeField]
        Image collapsedImage;

        [SerializeField]
        Image uncollapsedImage;

        override protected void Awake()
        {
            base.Awake();
            SetCollapsed(false);
        }


        public void SetCollapsed(bool value)
        {
            if(uncollapsedImage != null)
                uncollapsedImage.enabled = !value;

            if(collapsedImage != null)
                collapsedImage.enabled = value;
        }
    }
}