﻿using strange.extensions.signal.impl;

namespace com.bitvillain.entity
{
    public class EntityLockedSignal : Signal<int> { }
}