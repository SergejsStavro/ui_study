﻿using strange.extensions.signal.impl;

namespace com.bitvillain.entity
{
    public class UnlockRandomEntitySignal : Signal { }
}