﻿using strange.extensions.signal.impl;

namespace com.bitvillain.entity
{
    public class EntitySelectedSignal : Signal<int> { }
}