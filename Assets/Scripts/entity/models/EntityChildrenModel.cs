using System.Collections.Generic;

namespace com.bitvillain.entity
{
    public class EntityChildrenModel
    {
        public int ID;
        
        public List<int> ChildEntities;

        public EntityChildrenModel(
            int id
        )
        {
            ID = id;
            ChildEntities = new List<int>();
        }
    }
}