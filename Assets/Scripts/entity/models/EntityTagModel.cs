using System.Collections.Generic;

namespace com.bitvillain.entity
{
    public class EntityTagModel
    {
        public int ID;
        
        public List<string> Tags;

        public EntityTagModel(
            int id
        )
        {
            ID = id;
            Tags = new List<string>();
        }
    }
}