using System;

namespace com.bitvillain.entity
{
    public class EntityLockModel
    {
        public int ID;
        
        public bool IsNew;
        public bool IsLocked;

        public DateTime UnlockTimestamp;

        public EntityLockModel(
            int id,
            bool isLocked,
            bool isNew,
            DateTime unlockTimestamp
        )
        {
            ID = id;
            IsNew = isNew;
            IsLocked = isLocked;
            UnlockTimestamp = unlockTimestamp;
        }
    }
}