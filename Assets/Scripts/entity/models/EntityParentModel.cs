namespace com.bitvillain.entity
{
    public class EntityParentModel
    {
        public int ID;
        
        public int ParentEntity;

        public EntityParentModel(
            int id
        )
        {
            ID = id;
            ParentEntity = -1;
        }
    }
}