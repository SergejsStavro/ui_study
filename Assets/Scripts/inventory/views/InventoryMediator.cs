﻿using com.bitvillain.ui;
using strange.extensions.mediation.impl;

namespace com.bitvillain.inventory
{
    public class InventoryMediator : Mediator
    {
        [Inject]
        public InventoryView View { get; set; }

        [Inject]
        public SelectScreenSignal SelectScreenSignal { get; private set; }
        [Inject]
        public InventoryStartedSignal InventoryStartedSignal { get; set; }

        public override void OnRegister()
        {
            View.BackButtonClicked += OnBackButtonClicked;

            InventoryStartedSignal.AddListener(HandleInventoryStartedSignal);
        }

        private void HandleInventoryStartedSignal()
        {
            View.Initialize();
        }

        public override void OnRemove()
        {
            View.BackButtonClicked -= OnBackButtonClicked;

            InventoryStartedSignal.RemoveListener(HandleInventoryStartedSignal);
        }

        private void OnBackButtonClicked()
        {
            SelectScreenSignal.Dispatch(ScreenFSMCommand.Back);
        }
    }
}