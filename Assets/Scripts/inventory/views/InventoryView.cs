﻿using UnityEngine;
using strange.extensions.mediation.impl;
using UnityEngine.UI;
using System;

namespace com.bitvillain.inventory
{
    public class InventoryView : View
    {
        [SerializeField]
        Button backButton;

        public Action BackButtonClicked;

        override protected void OnEnable()
        {
            base.OnEnable();
            backButton.onClick.AddListener(OnBackButtonClicked);
        }

        internal void Initialize()
        {

        }

        override protected void OnDisable()
        {
            base.OnDisable();
            backButton.onClick.RemoveAllListeners();
        }

        private void OnBackButtonClicked()
        {
            if (BackButtonClicked != null)
                BackButtonClicked();
        }
    }
}