﻿using strange.extensions.command.impl;

namespace com.bitvillain.inventory
{
    public class StartInventoryCommand : Command
    {
        [Inject]
        public InventoryStartedSignal InventoryStartedSignal { get; set; }

        public override void Execute()
        {
            InventoryStartedSignal.Dispatch();
        }
    }
}