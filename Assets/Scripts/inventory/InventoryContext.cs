﻿using UnityEngine;

namespace com.bitvillain.inventory
{
    public class InventoryContext : SignalContext
    {
        public InventoryContext(MonoBehaviour view) : base(view) { }

        protected override void mapBindings()
        {
            base.mapBindings();

            mediationBinder.Bind<InventoryView>().To<InventoryMediator>();

            injectionBinder.Bind<InventoryStartedSignal>().ToSingleton();

            commandBinder
                .Bind<StartContextSignal>()
                .To<StartInventoryCommand>()
                .Once();
        }
    }
}