﻿using strange.extensions.context.impl;

namespace com.bitvillain.inventory
{
    public class InventoryContextView : ContextView
    {
        void Awake()
        {
            context = new InventoryContext(this);
            context.Start();
        }
    }   
}
