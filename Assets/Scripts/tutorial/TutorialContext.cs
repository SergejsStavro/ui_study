﻿using com.bitvillain.entity;
using UnityEngine;

namespace com.bitvillain.tutorial
{
    public class TutorialContext : SignalContext
    {
        public TutorialContext(MonoBehaviour view) : base(view) { }

        protected override void mapBindings()
        {
            base.mapBindings();

            injectionBinder
                .Bind<string>()
                .ToValue("data/tutorial/Entities")
                .ToName(EntitySetModel.ENTITY_DATA_PATH);

            injectionBinder
                .Bind<string>()
                .ToValue("data/tutorial/Tutorials")
                .ToName(TutorialSetModel.TUTORIAL_DATA_PATH);


            injectionBinder
                .Bind<string>()
                .ToValue("data/tutorial/EntityChildren")
                .ToName(EntitySetModel.ENTITY_CHILDREN_DATA_PATH);
                
            injectionBinder
                .Bind<string>()
                .ToValue("data/tutorial/EntityTags")
                .ToName(EntitySetModel.ENTITY_TAG_DATA_PATH);

            injectionBinder
                .Bind<string>()
                .ToValue("data/tutorial/EntityLocks")
                .ToName(EntitySetModel.ENTITY_LOCK_DATA_PATH);


            injectionBinder
                .Bind<EntitySetModel>()
                .ToSingleton();
                
            injectionBinder
                .Bind<TutorialSetModel>()
                .ToSingleton();
                
            mediationBinder
                .Bind<TutorialView>()
                .To<TutorialMediator>();
                
            injectionBinder
                .Bind<EntitySelectedSignal>()
                .ToSingleton();

            injectionBinder
                .Bind<EntityDeselectedSignal>()
                .ToSingleton();

            injectionBinder
                .Bind<TutorialSelectedSignal>()
                .ToSingleton();

            injectionBinder
                .Bind<TutorialDeselectedSignal>()
                .ToSingleton();

            commandBinder
                .Bind<SelectEntitySignal>()
                .To<SelectEntityCommand>()
                .To<RemoveEntityFromNewCommand>();

            injectionBinder
                .Bind<EntityRemovedFromNewSignal>()
                .ToSingleton();
            
            injectionBinder
                .Bind<EntityUnlockedSignal>()
                .ToSingleton();

            commandBinder
                .Bind<UnlockRandomEntitySignal>()
                .To<UnlockRandomEntityCommand>();

            injectionBinder
                .Bind<EntityLockedSignal>()
                .ToSingleton();

            commandBinder
                .Bind<LockRandomEntitySignal>()
                .To<LockRandomEntityCommand>();

            injectionBinder
                .Bind<TutorialStartedSignal>()
                .ToSingleton();

            commandBinder
                .Bind<StartContextSignal>()
                .InSequence()
                .To<InitializeEntitySetModelCommand>()
                .To<InitializeTutorialSetModelCommand>()
                .To<StartTutorialCommand>()
                .Once();
        }
    }
}