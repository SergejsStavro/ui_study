﻿using System;
using System.Collections.Generic;
using com.bitvillain.entity;
using strange.extensions.command.impl;
using UnityEngine;
using Random = UnityEngine.Random;

namespace com.bitvillain.tutorial
{
    public class LockRandomEntityCommand : Command
    {
        [Inject]
        public EntitySetModel EntitySetModel { get; set; }

        [Inject]
        public EntityLockedSignal EntityLockedSignal { get; set; }

        public override void Execute()
        {
            List<int> unlockedEntityIDs = new List<int>();

            foreach (var item in EntitySetModel.EntityLocks)
            {
                if (!item.Value.IsLocked)
                    unlockedEntityIDs.Add(item.Key);
            }
                

            if (unlockedEntityIDs.Count > 0)
            {
                int randomIndex = Random.Range(0, unlockedEntityIDs.Count);

                int lockedEntityID = unlockedEntityIDs[randomIndex];

                EntitySetModel.EntityLocks[lockedEntityID].IsLocked = true;
                EntitySetModel.EntityLocks[lockedEntityID].IsNew = true;
                EntitySetModel.EntityLocks[lockedEntityID].UnlockTimestamp = DateTime.MinValue;

                Debug.Log("Locked " + lockedEntityID + " of type " +  EntitySetModel.Entities[lockedEntityID].Type.ToString());

                EntityLockedSignal.Dispatch(lockedEntityID);
            }
        }
    }
}