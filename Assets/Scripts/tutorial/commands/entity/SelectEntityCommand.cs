﻿using com.bitvillain.entity;
using strange.extensions.command.impl;

namespace com.bitvillain.tutorial
{
    public class SelectEntityCommand : Command
    {
        [Inject]
        public EntitySetModel EntitySetModel { get; set; }

        [Inject]
        public int EntityID { get; set; }

        [Inject]
        public EntitySelectedSignal EntitySelectedSignal { get; set; }

        [Inject]
        public TutorialSelectedSignal TutorialSelectedSignal { get; set; }

        [Inject]
        public EntityDeselectedSignal EntityDeselectedSignal { get; set; }

        public override void Execute()
        {
            int deselectedEntityID = 
                EntitySetModel.SelectedEntityID;

            EntitySetModel.SelectedEntityID = EntityID;

            EntityDeselectedSignal.Dispatch(deselectedEntityID);
            EntitySelectedSignal.Dispatch(EntityID);

            if(EntitySetModel.Entities[EntityID].Type == EntityType.Tutorial)
                TutorialSelectedSignal.Dispatch(EntityID);
        }
    }
}