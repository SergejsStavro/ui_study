﻿// using strange.extensions.command.impl;

// namespace com.bitvillain.tutorial
// {
//     public class SelectTutorialCommand : Command
//     {
//         [Inject]
//         public TutorialSetModel TutorialModel { get; set; }

//         [Inject]
//         public int TutorialID { get; set; }

//         [Inject]
//         public TutorialSelectedSignal TutorialSelectedSignal { get; set; }

//         [Inject]
//         public TutorialDeselectedSignal TutorialDeselectedSignal { get; set; }

//         public override void Execute()
//         {
//             int deselectedTutorialID = 
//                 TutorialModel.SelectedTutorialID;

//             TutorialModel.SelectedTutorialID = TutorialID;

//             TutorialDeselectedSignal.Dispatch(deselectedTutorialID);
//             TutorialSelectedSignal.Dispatch(TutorialID);
//         }
//     }
// }