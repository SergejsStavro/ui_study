﻿using strange.extensions.signal.impl;

namespace com.bitvillain.tutorial
{
    public class TutorialStartedSignal : Signal { }
}