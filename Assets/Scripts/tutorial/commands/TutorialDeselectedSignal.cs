﻿using strange.extensions.signal.impl;

namespace com.bitvillain.tutorial
{
    public class TutorialDeselectedSignal : Signal<int> { }
}