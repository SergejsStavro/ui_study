﻿using System;
using System.Collections.Generic;
using com.bitvillain.services;
using strange.extensions.command.impl;

namespace com.bitvillain.tutorial
{
    public class InitializeTutorialSetModelCommand : Command
    {
        enum Columns
        {
            ID,
            Name,
            Description
        }

        [Inject]
        public TutorialSetModel TutorialSetModel { get; set; }
        
        [Inject(TutorialSetModel.TUTORIAL_DATA_PATH)]
        public string TutorialDataPath { get; private set; }

        [Inject]
        public CSVParserService CSVParserService { get; private set; }
        [Inject]
        public ResourceFileLoaderService ResourceFileLoaderService { get; private set; }

        public override void Execute()
        {
            var raw = ResourceFileLoaderService.LoadString(TutorialDataPath);
            var rows = CSVParserService.GetRowsFromString(raw, true);

            var tutorials = ParseRowsIntoTutorialModels(rows);

            TutorialSetModel.Tutorials = tutorials;

            //TODO load the rest from files
            // TutorialSetModel.NewTutorials =
            //     new List<int>(){
            //         1, 2
            //     };

            // TutorialSetModel.SelectedTutorialID = 0;
        }

        Dictionary<int, TutorialModel> ParseRowsIntoTutorialModels(List<string[]> rows)
        {
            var tutorials = new Dictionary<int, TutorialModel>();

            foreach (var row in rows)
            {
                var tutorial = ParseRowIntoTutorialModel(row);
                tutorials.Add(tutorial.ID, tutorial);
            }
            return tutorials;
        }

        TutorialModel ParseRowIntoTutorialModel(string[] row)
        {
            var tutorial = new TutorialModel(
                int.Parse(row[(int)Columns.ID]),
                row[(int)Columns.Name],
                row[(int)Columns.Description]
            );
            return tutorial;
        }
    }
}