﻿using strange.extensions.command.impl;

namespace com.bitvillain.tutorial
{
    public class StartTutorialCommand : Command
    {
        [Inject]
        public TutorialStartedSignal TutorialStartedSignal { get; set; }
        
        public override void Execute()
        {
            TutorialStartedSignal.Dispatch();
        }
    }
}