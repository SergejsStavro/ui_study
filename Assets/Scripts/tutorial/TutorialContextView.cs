﻿using strange.extensions.context.impl;

namespace com.bitvillain.tutorial
{
    public class TutorialContextView : ContextView
    {
        void Awake()
        {
            context = new TutorialContext(this);
            context.Start();
        }
    }   
}
