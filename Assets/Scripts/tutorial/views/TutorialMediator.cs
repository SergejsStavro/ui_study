﻿using UnityEngine;
using com.bitvillain.ui;
using strange.extensions.mediation.impl;
using System.Collections.Generic;
using System;
using com.bitvillain.entity;

namespace com.bitvillain.tutorial
{
    public class TutorialMediator : Mediator
    {
        [Inject]
        public TutorialView View { get; set; }

        [Inject]
        public EntitySetModel EntitySetModel { get; set; }

        [Inject]
        public TutorialSetModel TutorialSetModel { get; set; }

        [Inject]
        public SelectScreenSignal SelectScreenSignal { get; private set; }

        [Inject]
        public SelectEntitySignal SelectEntitySignal { get; private set; }
        [Inject]
        public EntitySelectedSignal EntitySelectedSignal { get; private set; }

        [Inject]
        public TutorialSelectedSignal TutorialSelectedSignal { get; private set; }

        [Inject]
        public EntityDeselectedSignal EntityDeselectedSignal { get; private set; }

        
        [Inject]
        public UnlockRandomEntitySignal UnlockRandomEntitySignal { get; private set; }
        [Inject]
        public LockRandomEntitySignal LockRandomEntitySignal { get; private set; }
        
        [Inject]
        public EntityUnlockedSignal EntityUnlockedSignal { get; private set; }
        [Inject]
        public EntityLockedSignal EntityLockedSignal { get; private set; }

        [Inject]
        public EntityRemovedFromNewSignal EntityRemovedFromNewSignal { get; private set; }

        [Inject]
        public TutorialStartedSignal TutorialStartedSignal { get; private set; }

        public override void OnRegister()
        {
            EntitySelectedSignal.AddListener(HandleEntitySelectedSignal);
            TutorialSelectedSignal.AddListener(HandleTutorialSelectedSignal);
            EntityDeselectedSignal.AddListener(HandleEntityDeselectedSignal);
            EntityRemovedFromNewSignal.AddListener(HandleEntityRemovedFromNewSignal);

            EntityUnlockedSignal.AddListener(HandleEntityUnlockedSignal);
            EntityLockedSignal.AddListener(HandleEntityLockedSignal);

            TutorialStartedSignal.AddListener(HandleTutorialStartedSignal);

            View.BackButtonClicked += OnBackButtonClicked;
            View.AddButtonClicked += OnAddButtonClicked;
            View.RemoveButtonClicked += OnRemoveButtonClicked;
            View.EntityButtonClicked += OnEntryButtonClicked;
        }

        public override void OnRemove()
        {
            EntitySelectedSignal.RemoveListener(HandleEntitySelectedSignal);
            TutorialSelectedSignal.RemoveListener(HandleTutorialSelectedSignal);
            EntityDeselectedSignal.RemoveListener(HandleEntityDeselectedSignal);
            EntityRemovedFromNewSignal.RemoveListener(HandleEntityRemovedFromNewSignal);

            EntityUnlockedSignal.RemoveListener(HandleEntityUnlockedSignal);
            EntityLockedSignal.RemoveListener(HandleEntityLockedSignal);

            TutorialStartedSignal.RemoveListener(HandleTutorialStartedSignal);

            View.BackButtonClicked -= OnBackButtonClicked;
            View.AddButtonClicked -= OnAddButtonClicked;
            View.RemoveButtonClicked -= OnRemoveButtonClicked;
            View.EntityButtonClicked -= OnEntryButtonClicked;
        }

        void HandleTutorialStartedSignal()
        {
            View.CreateEntityEntries(
                EntitySetModel.Entities,
                EntitySetModel.EntityParents,
                EntitySetModel.EntityChildren,
                EntitySetModel.EntityTags,
                EntitySetModel.EntityLocks
            );

            // View.MarkTutorialEntriesNew(TutorialSetModel.NewTutorials);

            View.SelectEntityEntry(EntitySetModel.SelectedEntityID);

            foreach (var entryKVP in TutorialSetModel.Tutorials)
                View.DisplayTutorialEntry(entryKVP.Key, entryKVP.Value.Name);   

            View.DisplayTutorialDetails(
                TutorialSetModel.Tutorials[EntitySetModel.SelectedEntityID].Name,
                TutorialSetModel.Tutorials[EntitySetModel.SelectedEntityID].Description
            );
        }
        
        private void HandleEntitySelectedSignal(int entityID)
        {
            View.SelectEntityEntry(entityID);
        }private void HandleEntityUnlockedSignal(int entityID)
        {
            View.AddEntityEntry(
                entityID,
                EntitySetModel.Entities[entityID],
                EntitySetModel.EntityParents[entityID],
                EntitySetModel.EntityChildren[entityID],
                EntitySetModel.EntityTags[entityID],
                EntitySetModel.EntityLocks[entityID]
            );

            if (TutorialSetModel.Tutorials.ContainsKey(entityID))
                View.DisplayTutorialEntry(
                    entityID,
                    TutorialSetModel.Tutorials[entityID].Name
                );
        }

        private void HandleEntityLockedSignal(int entityID)
        {
            View.RemoveEntityEntry(
                entityID
            );
        }

        private void HandleTutorialSelectedSignal(int entityID)
        {
            View.DisplayTutorialDetails(
                TutorialSetModel.Tutorials[entityID].Name,
                TutorialSetModel.Tutorials[entityID].Description
            );
        }

        private void HandleEntityDeselectedSignal(int entityID)
        {
            View.DeselectEntityEntry(entityID);
        }

        private void HandleEntityRemovedFromNewSignal(int entityID)
        {
            View.MarkEntityEntryOld(entityID);
        }

        private void OnBackButtonClicked()
        {
            SelectScreenSignal.Dispatch(ScreenFSMCommand.Back);
        }

        private void OnEntryButtonClicked(int entityID)
        {
            SelectEntitySignal.Dispatch(entityID);
        }

        private void OnAddButtonClicked()
        {
            UnlockRandomEntitySignal.Dispatch();
        }

        private void OnRemoveButtonClicked()
        {
            LockRandomEntitySignal.Dispatch();
        }

    }
}