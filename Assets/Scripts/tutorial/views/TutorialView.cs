﻿using UnityEngine;
using strange.extensions.mediation.impl;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using com.bitvillain.listScroll;
using com.bitvillain.entity;

namespace com.bitvillain.tutorial
{
    public class TutorialView : View
    {
        [SerializeField]
        Button backButton;

        [SerializeField]
        Button addButton;

        [SerializeField]
        Button removeButton;

        public Action BackButtonClicked;

        public Action AddButtonClicked;

        public Action RemoveButtonClicked;

        [SerializeField]
        ListScrollView listScrollView;

        [SerializeField]
        DetailScrollView detailScrollView;

        [SerializeField]
        GameObject entryPrefab;

        Dictionary<int, TutorialEntryView> tutorialEntryViews;

        public event Action<int> EntityButtonClicked
        {
            add
            {
                listScrollView.EntryButtonClicked += value;
            }
            remove
            {
                listScrollView.EntryButtonClicked -= value;
            }

        }

        override protected void OnEnable()
        {
            base.OnEnable();
            backButton.onClick.AddListener(OnBackButtonClicked);
            addButton.onClick.AddListener(OnAddButtonClicked);
            removeButton.onClick.AddListener(OnRemoveButtonClicked);
        }

        override protected void OnDisable()
        {
            base.OnDisable();
            backButton.onClick.RemoveAllListeners();
            addButton.onClick.RemoveAllListeners();
            removeButton.onClick.RemoveAllListeners();
        }

        private void OnBackButtonClicked()
        {
            if (BackButtonClicked != null)
                BackButtonClicked();
        }

        private void OnAddButtonClicked()
        {
            if (AddButtonClicked != null)
                AddButtonClicked();
        }

        private void OnRemoveButtonClicked()
        {
            if (RemoveButtonClicked != null)
                RemoveButtonClicked();
        }

        internal void CreateEntityEntries(
            Dictionary<int, EntityModel> entityModels,
            Dictionary<int, EntityParentModel> entityParents,
            Dictionary<int, EntityChildrenModel> entityChildren,
            Dictionary<int, EntityTagModel> entityTags,
            Dictionary<int, EntityLockModel> entityLocks
        )
        {
            tutorialEntryViews = new Dictionary<int, TutorialEntryView>();

            List<ListItemModel> listRecords =
                new List<ListItemModel>();

            int entityID;
            ListItemModel listRecord;
            foreach (var entityModelKVP in entityModels)
            {
                entityID = entityModelKVP.Value.ID;

                listRecord = CreateEntityEntry(
                    entityID,
                    entityModelKVP.Value,
                    entityParents[entityID],
                    entityChildren[entityID],
                    entityTags[entityID],
                    entityLocks[entityID]
                );

                if(listRecord != null)
                    listRecords.Add(listRecord);
            }

            listScrollView.AddEntries(listRecords);
        }

        internal ListItemModel CreateEntityEntry(
            int entityID,
            EntityModel entityModel,
            EntityParentModel entityParent,
            EntityChildrenModel entityChildren,
            EntityTagModel entityTags,
            EntityLockModel entityLock
        )
        {
            GameObject entryGameObject;
            ListItemModel listRecord = null;

            if (!entityLock.IsLocked)
            {
                entryGameObject = Instantiate<GameObject>(entryPrefab);

                LoadTutorialEntry(entityID, entryGameObject);

                listRecord = new ListItemModel(
                    entityID,
                    entryGameObject,
                    entityTags.Tags,
                    entityParent.ParentEntity,
                    entityChildren.ChildEntities,
                    entityLock.UnlockTimestamp,
                    entityLock.IsNew
                );

            }

            return listRecord;
        }

        internal void AddEntityEntry(
            int entityID,
            EntityModel entityModel,
            EntityParentModel entityParent,
            EntityChildrenModel entityChildren,
            EntityTagModel entityTags,
            EntityLockModel entityLock
        )
        {
            var listRecord = CreateEntityEntry(
                entityID,
                entityModel,
                entityParent,
                entityChildren,
                entityTags,
                entityLock
            );

            listScrollView.AddEntry(listRecord);
            listScrollView.SortEntries();
        }

        internal void RemoveEntityEntry(int entityID)
        {
            if(tutorialEntryViews.ContainsKey(entityID))
                tutorialEntryViews.Remove(entityID);
                
            listScrollView.RemoveEntry(entityID);
        }


        void LoadTutorialEntry(int entryID, GameObject entry)
        {
            var entryView = entry.GetComponent<TutorialEntryView>();
            tutorialEntryViews.Add(entryID, entryView);
        }

        internal void DisplayTutorialEntry(int entityID, string name)
        {
            if(tutorialEntryViews.ContainsKey(entityID))
                tutorialEntryViews[entityID].SetTitleText(name);
        }

        internal void DisplayTutorialDetails(string name, string description)
        {
            detailScrollView.SetHeaderText(name);
            detailScrollView.SetBodyText(description);
        }

        internal void SelectEntityEntry(int entityID)
        {
            listScrollView.SelectEntry(entityID);
        }

        internal void DeselectEntityEntry(int entityID)
        {
            listScrollView.DeselectEntry(entityID);
        }

        internal void MarkTutorialEntriesNew(List<int> entityIDs)
        {
            listScrollView.MarkEntriesNew(entityIDs);
        }

        internal void MarkEntityEntryOld(int entityID)
        {
            listScrollView.MarkEntryOld(entityID);
        }
    }
}