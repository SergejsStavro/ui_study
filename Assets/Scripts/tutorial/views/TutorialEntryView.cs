﻿using System;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;

namespace com.bitvillain.tutorial
{
    public class TutorialEntryView : View    
    {
        [SerializeField]
        Text titleText;

        public void SetTitleText(string text)
        {
            if(titleText != null)
                titleText.text = text;
        }
    }
}