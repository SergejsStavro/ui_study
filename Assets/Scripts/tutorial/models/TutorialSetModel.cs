using System.Collections.Generic;

namespace com.bitvillain.tutorial
{
    public class TutorialSetModel
    {
        public const string TUTORIAL_DATA_PATH = "TUTORIAL_DATA_PATH";

        public int SelectedTutorialID;

        public List<int> NewTutorials; //mutable

        public Dictionary<int, TutorialModel> Tutorials; //immutable
    }
}