namespace com.bitvillain.tutorial
{
    public struct TutorialModel
    {
        public int ID;
        public string Name;
        public string Description;

        public TutorialModel(
            int id,
            string name,
            string description
        )
        {
            ID = id;
            Name = name;
            Description = description;
        }
    }
}