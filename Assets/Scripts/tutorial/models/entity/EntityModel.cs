namespace com.bitvillain.tutorial
{
    public struct EntityModel
    {
        public int ID;
        public EntityType Type;

        public EntityModel(
            int id,
            EntityType type
        )
        {
            ID = id;
            Type = type;
        }
    }
}