﻿using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;

namespace com.bitvillain.tutorial
{
    public class DetailScrollView : View
    {
        [SerializeField]
        Text header;

        [SerializeField]
        Text body;

        public void SetHeaderText(string text)
        {
            header.text = text;
        }

        public void SetBodyText(string text)
        {
            body.text = text;

            //TODO reset scroll bar position
        }
    }
}