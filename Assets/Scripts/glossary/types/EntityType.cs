﻿namespace com.bitvillain.glossary
{
    public enum EntityType
    {
        None = 0,
        Location = 1,
        Character = 2,
        Event = 3,
        PointOfInterest = 4,
        EntityGroup = 5,
    }   
}