using System.Collections.Generic;

namespace com.bitvillain.glossary
{
    public class LocationSetModel
    {
        public const string LOCATION_DATA_PATH = "LOCATION_DATA_PATH";

        public Dictionary<int, LocationModel> Locations;
    }
}