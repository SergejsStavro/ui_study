using System.Collections.Generic;

namespace com.bitvillain.glossary
{
    public class EntityGroupSetModel
    {
        public const string ENTITY_GROUP_DATA_PATH = "ENTITY_GROUP_DATA_PATH";

        public Dictionary<int, EntityGroupModel> EntityGroups;
    }
}