namespace com.bitvillain.glossary
{
    public struct CharacterModel
    {
        public int ID;
        public string Name;
        public string ImagePath;
        public string Description;

        public CharacterModel(
            int id,
            string name,
            string imagePath,
            string description
        )
        {
            ID = id;
            Name = name;
            ImagePath = imagePath;
            Description = description;
        }
    }
}