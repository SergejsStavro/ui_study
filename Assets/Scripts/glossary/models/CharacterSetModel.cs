using System.Collections.Generic;

namespace com.bitvillain.glossary
{
    public class CharacterSetModel
    {
        public const string CHARACTER_DATA_PATH = "CHARACTER_DATA_PATH";

        public Dictionary<int, CharacterModel> Characters; //immutable
    }
}