using System.Collections.Generic;

namespace com.bitvillain.glossary
{
    public class EventSetModel
    {
        public const string EVENT_DATA_PATH = "EVENT_DATA_PATH";

        public Dictionary<int, EventModel> Events;
    }
}