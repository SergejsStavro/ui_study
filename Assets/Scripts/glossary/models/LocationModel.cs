namespace com.bitvillain.glossary
{
    public struct LocationModel
    {
        public int ID;
        public string Name;
        public string Description;

        public LocationModel(
            int id,
            string name,
            string description
        )
        {
            ID = id;
            Name = name;
            Description = description;
        }
    }
}