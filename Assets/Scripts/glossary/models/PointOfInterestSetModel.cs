using System.Collections.Generic;

namespace com.bitvillain.glossary
{
    public class PointOfInterestSetModel
    {
        public const string POINT_OF_INTEREST_DATA_PATH = "POINT_OF_INTEREST_DATA_PATH";
        
        public Dictionary<int, PointOfInterestModel> PointsOfInterest;
    }
}