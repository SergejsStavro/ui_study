namespace com.bitvillain.glossary
{
    public struct EntityModel
    {
        public int ID;
        public EntityType Type;
        public string ImagePath;

        public EntityModel(
            int id,
            EntityType type,
            string imagePath
        )
        {
            ID = id;
            Type = type;
            ImagePath = imagePath;
        }
    }
}