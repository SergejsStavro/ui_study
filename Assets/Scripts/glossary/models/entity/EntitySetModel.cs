using System.Collections.Generic;
using com.bitvillain.entity;

namespace com.bitvillain.glossary
{
    public class EntitySetModel
    {
        public const string ENTITY_DATA_PATH = "ENTITY_DATA_PATH";
        public const string ENTITY_CHILDREN_DATA_PATH = "ENTITY_CHILDREN_DATA_PATH";
        public const string ENTITY_TAG_DATA_PATH = "ENTITY_TAG_DATA_PATH";
        public const string ENTITY_LOCK_DATA_PATH = "ENTITY_LOCK_DATA_PATH";
        
        public Dictionary<int, EntityModel> Entities;
        public Dictionary<int, EntityParentModel> EntityParents; //derived from children
        public Dictionary<int, EntityChildrenModel> EntityChildren;
        public Dictionary<int, EntityTagModel> EntityTags;
        public Dictionary<int, EntityLockModel> EntityLocks;
        
        internal int SelectedEntityID;
    }

}