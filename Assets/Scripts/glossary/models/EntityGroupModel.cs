namespace com.bitvillain.glossary
{
    public class EntityGroupModel
    {
        public int ID;
        public EntityType Type;

        public EntityGroupModel(
            int id,
            EntityType type
        )
        {
            ID = id;
            Type = type;
        }
    }
}