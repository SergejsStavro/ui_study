namespace com.bitvillain.glossary
{
    public struct EventModel
    {
        public int ID;
        public string Name;
        public string Description;

        public EventModel(
            int id,
            string name,
            string description
        )
        {
            ID = id;
            Name = name;
            Description = description;
        }
    }
}