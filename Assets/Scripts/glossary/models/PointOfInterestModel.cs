namespace com.bitvillain.glossary
{
    public struct PointOfInterestModel
    {
        public int ID;
        public string Name;
        public string Description;

        public PointOfInterestModel(
            int id,
            string name,
            string description
        )
        {
            ID = id;
            Name = name;
            Description = description;
        }
    }
}