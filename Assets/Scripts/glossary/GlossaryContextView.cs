﻿using strange.extensions.context.impl;

namespace com.bitvillain.glossary
{
    public class GlossaryContextView : ContextView
    {
        void Awake()
        {
            context = new GlossaryContext(this);
            context.Start();
        }
    }   
}
