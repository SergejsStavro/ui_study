﻿using System.Collections.Generic;
using com.bitvillain.services;
using strange.extensions.command.impl;

namespace com.bitvillain.glossary
{
    public class InitializeLocationSetModelCommand : Command
    {
        enum Columns
        {
            ID,
            Name,
            Description
        }


        [Inject]
        public CSVParserService CSVParserService { get; private set; }
        [Inject]
        public ResourceFileLoaderService ResourceFileLoaderService { get; private set; }
        

        [Inject(LocationSetModel.LOCATION_DATA_PATH)]
        public string LocationDataPath { get; private set; }
        
        [Inject]
        public LocationSetModel LocationSetModel { get; private set; }

        public override void Execute()
        {
            string raw;
            List<string[]> rows;

            raw = ResourceFileLoaderService.LoadString(LocationDataPath);
            rows = CSVParserService.GetRowsFromString(raw, true);

            LocationSetModel.Locations = 
                ParseRowsIntoLocationModels(rows);
        }

        Dictionary<int, LocationModel> ParseRowsIntoLocationModels(List<string[]> rows)
        {
            var Locations = new Dictionary<int, LocationModel>();

            foreach (var row in rows)
            {
                var Location = ParseRowIntoLocationModel(row);
                Locations.Add(Location.ID, Location);
            }
            return Locations;
        }

        LocationModel ParseRowIntoLocationModel(string[] row)
        {
            var Location = new LocationModel(
                int.Parse(row[(int)Columns.ID]),
                row[(int)Columns.Name],
                row[(int)Columns.Description]
            );
            return Location;
        }
    }
}