﻿using System.Collections.Generic;
using com.bitvillain.services;
using strange.extensions.command.impl;

namespace com.bitvillain.glossary
{
    public class InitializeEntityGroupSetModelCommand : Command
    {
        enum Columns
        {
            ID,
            Type,
        }


        [Inject]
        public CSVParserService CSVParserService { get; private set; }
        [Inject]
        public ResourceFileLoaderService ResourceFileLoaderService { get; private set; }
        

        [Inject(EntityGroupSetModel.ENTITY_GROUP_DATA_PATH)]
        public string EntityGroupDataPath { get; private set; }
        
        [Inject]
        public EntityGroupSetModel EntityGroupSetModel { get; private set; }

        public override void Execute()
        {
            string raw;
            List<string[]> rows;

            raw = ResourceFileLoaderService.LoadString(EntityGroupDataPath);
            rows = CSVParserService.GetRowsFromString(raw, true);

            EntityGroupSetModel.EntityGroups = 
                ParseRowsIntoEntityGroupModels(rows);
        }

        Dictionary<int, EntityGroupModel> ParseRowsIntoEntityGroupModels(List<string[]> rows)
        {
            var EntityGroups = new Dictionary<int, EntityGroupModel>();

            foreach (var row in rows)
            {
                var EntityGroup = ParseRowIntoEntityGroupModel(row);
                EntityGroups.Add(EntityGroup.ID, EntityGroup);
            }
            return EntityGroups;
        }

        EntityGroupModel ParseRowIntoEntityGroupModel(string[] row)
        {
            var EntityGroup = new EntityGroupModel(
                int.Parse(row[(int)Columns.ID]),
                Utils.GetEnumElementFromName<EntityType>(
                    row[(int)Columns.Type]
                )
            );
            return EntityGroup;
        }
    }
}