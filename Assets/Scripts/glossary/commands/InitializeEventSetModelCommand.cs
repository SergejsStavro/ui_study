﻿using System.Collections.Generic;
using com.bitvillain.services;
using strange.extensions.command.impl;

namespace com.bitvillain.glossary
{
    public class InitializeEventSetModelCommand : Command
    {
        enum Columns
        {
            ID,
            Name,
            Description
        }
        
        [Inject]
        public EventSetModel EventSetModel { get; private set; }

        [Inject(EventSetModel.EVENT_DATA_PATH)]
        public string EventDataPath { get; private set; }

        [Inject]
        public CSVParserService CSVParserService { get; private set; }
        [Inject]
        public ResourceFileLoaderService ResourceFileLoaderService { get; private set; }

        public override void Execute()
        {
            string raw;
            List<string[]> rows;

            raw = ResourceFileLoaderService.LoadString(EventDataPath);
            rows = CSVParserService.GetRowsFromString(raw, true);

            EventSetModel.Events = 
                ParseRowsIntoEventModels(rows);
        }

        Dictionary<int, EventModel> ParseRowsIntoEventModels(List<string[]> rows)
        {
            var Events = new Dictionary<int, EventModel>();

            foreach (var row in rows)
            {
                var Event = ParseRowIntoEventModel(row);
                Events.Add(Event.ID, Event);
            }
            return Events;
        }

        EventModel ParseRowIntoEventModel(string[] row)
        {
            var Event = new EventModel(
                int.Parse(row[(int)Columns.ID]),
                row[(int)Columns.Name],
                row[(int)Columns.Description]
            );
            return Event;
        }
    }
}