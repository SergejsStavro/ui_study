﻿using System.Collections.Generic;
using com.bitvillain.services;
using strange.extensions.command.impl;

namespace com.bitvillain.glossary
{
    public class InitializePointOfInterestSetModelCommand : Command
    {
        enum Columns
        {
            ID,
            Name,
            Description
        }
        
        [Inject]
        public PointOfInterestSetModel PointOfInterestSetModel { get; private set; }

        [Inject(PointOfInterestSetModel.POINT_OF_INTEREST_DATA_PATH)]
        public string PointOfInterestDataPath { get; private set; }

        [Inject]
        public CSVParserService CSVParserService { get; private set; }
        [Inject]
        public ResourceFileLoaderService ResourceFileLoaderService { get; private set; }

        public override void Execute()
        {
            string raw;
            List<string[]> rows;

            raw = ResourceFileLoaderService.LoadString(PointOfInterestDataPath);
            rows = CSVParserService.GetRowsFromString(raw, true);

            PointOfInterestSetModel.PointsOfInterest = 
                ParseRowsIntoPointOfInterestModels(rows);
        }

        Dictionary<int, PointOfInterestModel> ParseRowsIntoPointOfInterestModels(List<string[]> rows)
        {
            var PointOfInterests = new Dictionary<int, PointOfInterestModel>();

            foreach (var row in rows)
            {
                var PointOfInterest = ParseRowIntoPointOfInterestModel(row);
                PointOfInterests.Add(PointOfInterest.ID, PointOfInterest);
            }
            return PointOfInterests;
        }

        PointOfInterestModel ParseRowIntoPointOfInterestModel(string[] row)
        {
            var PointOfInterest = new PointOfInterestModel(
                int.Parse(row[(int)Columns.ID]),
                row[(int)Columns.Name],
                row[(int)Columns.Description]
            );
            return PointOfInterest;
        }
    }
}