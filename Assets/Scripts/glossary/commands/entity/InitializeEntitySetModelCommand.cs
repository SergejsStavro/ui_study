﻿using System;
using System.Collections.Generic;
using com.bitvillain.entity;
using com.bitvillain.services;
using strange.extensions.command.impl;

namespace com.bitvillain.glossary
{
    public class InitializeEntitySetModelCommand : Command
    {
        enum EntityColumns
        {
            ID,
            Type,
            ImagePath
        }

        enum EntityLockColumns
        {
            ID,
            IsLocked,
            IsNew,
            UnlockTimestamp,
        }


        [Inject]
        public CSVParserService CSVParserService { get; private set; }
        [Inject]
        public ResourceFileLoaderService ResourceFileLoaderService { get; private set; }


        [Inject(EntitySetModel.ENTITY_DATA_PATH)]
        public string EntityDataPath { get; private set; }

        [Inject(EntitySetModel.ENTITY_CHILDREN_DATA_PATH)]
        public string EntityChildrenDataPath { get; private set; }

        [Inject(EntitySetModel.ENTITY_TAG_DATA_PATH)]
        public string EntityTagDataPath { get; private set; }

        [Inject(EntitySetModel.ENTITY_LOCK_DATA_PATH)]
        public string EntityLockDataPath { get; private set; }
        


        [Inject]
        public EntitySetModel EntitySetModel { get; set; }

        public override void Execute()
        {
            string raw;
            List<string[]> rows;

            raw = ResourceFileLoaderService.LoadString(EntityDataPath);
            rows = CSVParserService.GetRowsFromString(raw, true);

            EntitySetModel.Entities =
                ParseRowsIntoEntityModels(rows);

            EntitySetModel.SelectedEntityID = 0;

            raw = ResourceFileLoaderService.LoadString(EntityChildrenDataPath);
            rows = CSVParserService.GetRowsFromString(raw, true);

            LoadEntityChildren(rows);


            LoadEntityParents();

            raw = ResourceFileLoaderService.LoadString(EntityTagDataPath);
            rows = CSVParserService.GetRowsFromString(raw, true);

            LoadEntityTags(rows);


            raw = ResourceFileLoaderService.LoadString(EntityLockDataPath);
            rows = CSVParserService.GetRowsFromString(raw, true);

            EntitySetModel.EntityLocks =
                ParseRowsIntoEntityLockModels(rows);
        }

        private void LoadEntityParents()
        {
            EntitySetModel.EntityParents = new Dictionary<int, EntityParentModel>();
            foreach (var entityKVP in EntitySetModel.Entities)
            {
                EntitySetModel.EntityParents.Add(
                    entityKVP.Key, new EntityParentModel(entityKVP.Key));
            }

            int childID;
            int parentID;
            foreach (var entityKVP in EntitySetModel.EntityChildren)
            {
                parentID = entityKVP.Key;
                for (int i = 0; i < entityKVP.Value.ChildEntities.Count; i++)
                {
                    childID = entityKVP.Value.ChildEntities[i];

                    if(EntitySetModel.EntityParents.ContainsKey(childID))
                        EntitySetModel.EntityParents[childID].ParentEntity = parentID;
                }
            }
        }

        void LoadEntityChildren(List<string[]> rows)
        {
            EntitySetModel.EntityChildren = new Dictionary<int, EntityChildrenModel>();
            foreach (var entityKVP in EntitySetModel.Entities)
            {
                EntitySetModel.EntityChildren.Add(
                    entityKVP.Key, new EntityChildrenModel(entityKVP.Key));
            }

            Dictionary<int, List<int>> entities;

            entities = ParseRowsIntoEntityChildrenList(rows);
            foreach (var item in entities)
            {
                EntitySetModel
                    .EntityChildren[item.Key]
                        .ChildEntities.AddRange(item.Value);
            }
        }

        void LoadEntityTags(List<string[]> rows)
        {
            EntitySetModel.EntityTags = new Dictionary<int, EntityTagModel>();
            foreach (var entityKVP in EntitySetModel.Entities)
            {
                EntitySetModel.EntityTags.Add(
                    entityKVP.Key, new EntityTagModel(entityKVP.Key));
            }

            Dictionary<int, List<string>> entities;

            entities = ParseRowsIntoEntityTagList(rows);
            foreach (var item in entities)
            {
                EntitySetModel
                    .EntityTags[item.Key]
                        .Tags.AddRange(item.Value);
            }
        }

        private Dictionary<int, List<int>> ParseRowsIntoEntityChildrenList(List<string[]> rows)
        {
            var entityChildList = new Dictionary<int, List<int>>();

            foreach (var row in rows)
            {
                var parentID = int.Parse(row[0]);
                var childID = int.Parse(row[1]);

                if (entityChildList.ContainsKey(parentID))
                    entityChildList[parentID].Add(childID);
                else
                    entityChildList.Add(
                        parentID,
                        new List<int>(){
                            childID
                        }
                    );
            }
            return entityChildList;
        }

        private Dictionary<int, List<string>> ParseRowsIntoEntityTagList(List<string[]> rows)
        {
            var entityTagList = new Dictionary<int, List<string>>();

            foreach (var row in rows)
            {
                var entityID = int.Parse(row[0]);
                var tag = row[1];

                if (entityTagList.ContainsKey(entityID))
                    entityTagList[entityID].Add(tag);
                else
                    entityTagList.Add(
                        entityID,
                        new List<string>(){
                            tag
                        }
                    );
            }
            return entityTagList;
        }
        

        Dictionary<int, EntityLockModel> ParseRowsIntoEntityLockModels(List<string[]> rows)
        {
            var entities = new Dictionary<int, EntityLockModel>();

            foreach (var row in rows)
            {
                var Entity = ParseRowIntoEntityLockModel(row);
                entities.Add(Entity.ID, Entity);
            }
            return entities;
        }

        EntityLockModel ParseRowIntoEntityLockModel(string[] row)
        {
            var entity = new EntityLockModel(
                int.Parse(row[(int)EntityLockColumns.ID]),
                bool.Parse(row[(int)EntityLockColumns.IsLocked]),
                bool.Parse(row[(int)EntityLockColumns.IsNew]),
                DateTime.Parse(row[(int)EntityLockColumns.UnlockTimestamp])
            );
            return entity;
        }

        Dictionary<int, EntityModel> ParseRowsIntoEntityModels(List<string[]> rows)
        {
            var entities = new Dictionary<int, EntityModel>();

            foreach (var row in rows)
            {
                var Entity = ParseRowIntoEntityModel(row);
                entities.Add(Entity.ID, Entity);
            }
            return entities;
        }

        EntityModel ParseRowIntoEntityModel(string[] row)
        {
            var entity = new EntityModel(
                int.Parse(row[(int)EntityColumns.ID]),
                Utils.GetEnumElementFromName<EntityType>(
                    row[(int)EntityColumns.Type]
                ),
                row[(int)EntityColumns.ImagePath]
            );
            return entity;
        }
    }
}