﻿using com.bitvillain.entity;
using strange.extensions.command.impl;

namespace com.bitvillain.glossary
{
    public class RemoveEntityFromNewCommand : Command
    {
        [Inject]
        public EntitySetModel EntitySetModel { get; set; }

        [Inject]
        public int EntityID { get; set; }

        [Inject]
        public EntityRemovedFromNewSignal EntityRemovedFromNewSignal { get; set; }

        public override void Execute()
        {
            if (EntitySetModel.EntityLocks.ContainsKey(EntityID) &&
                EntitySetModel.EntityLocks[EntityID].IsNew
            )
            {
                EntitySetModel.EntityLocks[EntityID].IsNew = false;

                EntityRemovedFromNewSignal.Dispatch(EntityID);
            }
        }
    }
}