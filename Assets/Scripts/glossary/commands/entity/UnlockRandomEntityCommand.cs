﻿using System;
using System.Collections.Generic;
using com.bitvillain.entity;
using strange.extensions.command.impl;
using UnityEngine;
using Random = UnityEngine.Random;

namespace com.bitvillain.glossary
{
    public class UnlockRandomEntityCommand : Command
    {
        [Inject]
        public EntitySetModel EntitySetModel { get; set; }

        [Inject]
        public EntityUnlockedSignal EntityUnlockedSignal { get; set; }

        public override void Execute()
        {
            List<int> lockedEntityIDs = new List<int>();

            foreach (var item in EntitySetModel.EntityLocks)
            {
                if (item.Value.IsLocked)
                    lockedEntityIDs.Add(item.Key);
            }
                

            if (lockedEntityIDs.Count > 0)
            {
                int randomIndex = Random.Range(0, lockedEntityIDs.Count);

                int unlockedEntityID = lockedEntityIDs[randomIndex];

                EntitySetModel.EntityLocks[unlockedEntityID].IsLocked = false;
                EntitySetModel.EntityLocks[unlockedEntityID].IsNew = true;
                EntitySetModel.EntityLocks[unlockedEntityID].UnlockTimestamp = DateTime.Now;

                Debug.Log("Unlocked " + unlockedEntityID + " of type " +  EntitySetModel.Entities[unlockedEntityID].Type.ToString());

                EntityUnlockedSignal.Dispatch(unlockedEntityID);
            }
        }
    }
}