﻿using com.bitvillain.entity;
using strange.extensions.command.impl;

namespace com.bitvillain.glossary
{
    public class SelectEntityCommand : Command
    {
        [Inject]
        public EntitySetModel EntitySetModel { get; set; }

        [Inject]
        public int EntityID { get; set; }

        [Inject]
        public EntitySelectedSignal EntitySelectedSignal { get; set; }

        [Inject]
        public LocationSelectedSignal LocationSelectedSignal { get; set; }
        [Inject]
        public CharacterSelectedSignal CharacterSelectedSignal { get; set; }
        [Inject]
        public EventSelectedSignal EventSelectedSignal { get; set; }
        [Inject]
        public PointOfInterestSelectedSignal PointOfInterestSelectedSignal { get; set; }

        [Inject]
        public EntityDeselectedSignal EntityDeselectedSignal { get; set; }

        public override void Execute()
        {
            int deselectedEntityID = 
                EntitySetModel.SelectedEntityID;

            EntitySetModel.SelectedEntityID = EntityID;

            EntityDeselectedSignal.Dispatch(deselectedEntityID);
            EntitySelectedSignal.Dispatch(EntityID);

            if(EntitySetModel.Entities[EntityID].Type == EntityType.Location)
                LocationSelectedSignal.Dispatch(EntityID);

            if(EntitySetModel.Entities[EntityID].Type == EntityType.Character)
                CharacterSelectedSignal.Dispatch(EntityID);

            if(EntitySetModel.Entities[EntityID].Type == EntityType.Event)
                EventSelectedSignal.Dispatch(EntityID);

            if(EntitySetModel.Entities[EntityID].Type == EntityType.PointOfInterest)
                PointOfInterestSelectedSignal.Dispatch(EntityID);
        }
    }
}