﻿using System.Collections.Generic;
using com.bitvillain.services;
using strange.extensions.command.impl;

namespace com.bitvillain.glossary
{
    public class InitializeCharacterSetModelCommand : Command
    {
        enum Columns
        {
            ID,
            Name,
            ImagePath,
            Description
        }
        
        [Inject]
        public CharacterSetModel CharacterSetModel { get; private set; }

        [Inject(CharacterSetModel.CHARACTER_DATA_PATH)]
        public string CharacterDataPath { get; private set; }

        [Inject]
        public CSVParserService CSVParserService { get; private set; }
        [Inject]
        public ResourceFileLoaderService ResourceFileLoaderService { get; private set; }

        public override void Execute()
        {
            string raw;
            List<string[]> rows;

            raw = ResourceFileLoaderService.LoadString(CharacterDataPath);
            rows = CSVParserService.GetRowsFromString(raw, true);

            CharacterSetModel.Characters = 
                ParseRowsIntoCharacterModels(rows);
        }

        Dictionary<int, CharacterModel> ParseRowsIntoCharacterModels(List<string[]> rows)
        {
            var characters = new Dictionary<int, CharacterModel>();

            foreach (var row in rows)
            {
                var character = ParseRowIntoCharacterModel(row);
                characters.Add(character.ID, character);
            }
            return characters;
        }

        CharacterModel ParseRowIntoCharacterModel(string[] row)
        {
            var character = new CharacterModel(
                int.Parse(row[(int)Columns.ID]),
                row[(int)Columns.Name],
                row[(int)Columns.ImagePath],
                row[(int)Columns.Description]
            );
            return character;
        }
    }
}