﻿using strange.extensions.signal.impl;

namespace com.bitvillain.glossary
{
    public class GlossaryStartedSignal : Signal { }
}