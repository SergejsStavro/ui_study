﻿using strange.extensions.command.impl;

namespace com.bitvillain.glossary
{
    public class StartGlossaryCommand : Command
    {
        [Inject]
        public GlossaryStartedSignal GlossaryStartedSignal { get; set; }

        public override void Execute()
        {
            GlossaryStartedSignal.Dispatch();
        }
    }
}