﻿using com.bitvillain.entity;
using UnityEngine;

namespace com.bitvillain.glossary
{
    public class GlossaryContext : SignalContext
    {
        public GlossaryContext(MonoBehaviour view) : base(view) { }

        protected override void mapBindings()
        {
            base.mapBindings();

            injectionBinder
                .Bind<string>()
                .ToValue("data/glossary/Entities")
                .ToName(EntitySetModel.ENTITY_DATA_PATH);

            injectionBinder
                .Bind<string>()
                .ToValue("data/glossary/EntityGroups")
                .ToName(EntityGroupSetModel.ENTITY_GROUP_DATA_PATH);


            injectionBinder
                .Bind<string>()
                .ToValue("data/glossary/Locations")
                .ToName(LocationSetModel.LOCATION_DATA_PATH);

            injectionBinder
                .Bind<string>()
                .ToValue("data/glossary/Characters")
                .ToName(CharacterSetModel.CHARACTER_DATA_PATH);

            injectionBinder
                .Bind<string>()
                .ToValue("data/glossary/Events")
                .ToName(EventSetModel.EVENT_DATA_PATH);

            injectionBinder
                .Bind<string>()
                .ToValue("data/glossary/PointsOfInterest")
                .ToName(PointOfInterestSetModel.POINT_OF_INTEREST_DATA_PATH);
                

            injectionBinder
                .Bind<string>()
                .ToValue("data/glossary/EntityChildren")
                .ToName(EntitySetModel.ENTITY_CHILDREN_DATA_PATH);
                
            injectionBinder
                .Bind<string>()
                .ToValue("data/glossary/EntityTags")
                .ToName(EntitySetModel.ENTITY_TAG_DATA_PATH);

            injectionBinder
                .Bind<string>()
                .ToValue("data/glossary/EntityLocks")
                .ToName(EntitySetModel.ENTITY_LOCK_DATA_PATH);


            injectionBinder
                .Bind<EntitySetModel>()
                .ToSingleton();

            injectionBinder
                .Bind<LocationSetModel>()
                .ToSingleton();

            injectionBinder
                .Bind<EntityGroupSetModel>()
                .ToSingleton();

            injectionBinder
                .Bind<CharacterSetModel>()
                .ToSingleton();

            injectionBinder
                .Bind<EventSetModel>()
                .ToSingleton();

            injectionBinder
                .Bind<PointOfInterestSetModel>()
                .ToSingleton();

            mediationBinder.Bind<GlossaryView>().To<GlossaryMediator>();

            injectionBinder
                .Bind<EntitySelectedSignal>()
                .ToSingleton();

            injectionBinder
                .Bind<EntityDeselectedSignal>()
                .ToSingleton();

            injectionBinder
                .Bind<LocationSelectedSignal>()
                .ToSingleton();

            injectionBinder
                .Bind<CharacterSelectedSignal>()
                .ToSingleton();

            injectionBinder
                .Bind<EventSelectedSignal>()
                .ToSingleton();

            injectionBinder
                .Bind<PointOfInterestSelectedSignal>()
                .ToSingleton();

            commandBinder
                .Bind<SelectEntitySignal>()
                .To<SelectEntityCommand>()
                .To<RemoveEntityFromNewCommand>();

            injectionBinder
                .Bind<EntityRemovedFromNewSignal>()
                .ToSingleton();

            
            injectionBinder
                .Bind<EntityUnlockedSignal>()
                .ToSingleton();

            commandBinder
                .Bind<UnlockRandomEntitySignal>()
                .To<UnlockRandomEntityCommand>();

            injectionBinder
                .Bind<EntityLockedSignal>()
                .ToSingleton();

            commandBinder
                .Bind<LockRandomEntitySignal>()
                .To<LockRandomEntityCommand>();

            injectionBinder
                .Bind<GlossaryStartedSignal>()
                .ToSingleton();

            commandBinder
                .Bind<StartContextSignal>()
                .InSequence()
                .To<InitializeEntitySetModelCommand>()
                .To<InitializeEntityGroupSetModelCommand>()
                .To<InitializeLocationSetModelCommand>()
                .To<InitializeCharacterSetModelCommand>()
                .To<InitializeEventSetModelCommand>()
                .To<InitializePointOfInterestSetModelCommand>()
                .To<StartGlossaryCommand>()
                .Once();
        }
    }
}