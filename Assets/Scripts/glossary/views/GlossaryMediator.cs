﻿using UnityEngine;
using com.bitvillain.ui;
using strange.extensions.mediation.impl;
using System.Collections.Generic;
using System;
using com.bitvillain.entity;

namespace com.bitvillain.glossary
{
    public class GlossaryMediator : Mediator
    {
        [Inject]
        public GlossaryView View { get; set; }

        [Inject]
        public EntitySetModel EntitySetModel { get; set; }

        [Inject]
        public LocationSetModel LocationSetModel { get; set; }

        [Inject]
        public CharacterSetModel CharacterSetModel { get; set; }

        [Inject]
        public EventSetModel EventSetModel { get; set; }
        [Inject]
        public PointOfInterestSetModel PointOfInterestSetModel { get; set; }
        [Inject]
        public EntityGroupSetModel EntityGroupSetModel { get; set; }

        [Inject]
        public SelectScreenSignal SelectScreenSignal { get; private set; }

        [Inject]
        public SelectEntitySignal SelectEntitySignal { get; private set; }
        [Inject]
        public EntitySelectedSignal EntitySelectedSignal { get; private set; }
        [Inject]
        public LocationSelectedSignal LocationSelectedSignal { get; private set; }
        [Inject]
        public CharacterSelectedSignal CharacterSelectedSignal { get; private set; }
        [Inject]
        public EventSelectedSignal EventSelectedSignal { get; private set; }
        [Inject]
        public PointOfInterestSelectedSignal PointOfInterestSelectedSignal { get; private set; }

        [Inject]
        public EntityDeselectedSignal EntityDeselectedSignal { get; private set; }

        [Inject]
        public UnlockRandomEntitySignal UnlockRandomEntitySignal { get; private set; }
        [Inject]
        public LockRandomEntitySignal LockRandomEntitySignal { get; private set; }

        [Inject]
        public EntityUnlockedSignal EntityUnlockedSignal { get; private set; }
        [Inject]
        public EntityLockedSignal EntityLockedSignal { get; private set; }

        [Inject]
        public EntityRemovedFromNewSignal EntityRemovedFromNewSignal { get; private set; }

        [Inject]
        public GlossaryStartedSignal GlossaryStartedSignal { get; private set; }

        public override void OnRegister()
        {
            EntitySelectedSignal.AddListener(HandleEntitySelectedSignal);
            LocationSelectedSignal.AddListener(HandleLocationSelectedSignal);
            CharacterSelectedSignal.AddListener(HandleCharacterSelectedSignal);
            EventSelectedSignal.AddListener(HandleEventSelectedSignal);
            PointOfInterestSelectedSignal.AddListener(HandlePointOfInterestSelectedSignal);
            EntityDeselectedSignal.AddListener(HandleEntityDeselectedSignal);
            EntityRemovedFromNewSignal.AddListener(HandleEntityRemovedFromNewSignal);

            EntityUnlockedSignal.AddListener(HandleEntityUnlockedSignal);
            EntityLockedSignal.AddListener(HandleEntityLockedSignal);

            GlossaryStartedSignal.AddListener(HandleGlossaryStartedSignal);

            View.BackButtonClicked += OnBackButtonClicked;
            View.AddButtonClicked += OnAddButtonClicked;
            View.RemoveButtonClicked += OnRemoveButtonClicked;
            View.EntityButtonClicked += OnEntityButtonClicked;
        }

        public override void OnRemove()
        {
            EntitySelectedSignal.RemoveListener(HandleEntitySelectedSignal);
            LocationSelectedSignal.RemoveListener(HandleLocationSelectedSignal);
            CharacterSelectedSignal.RemoveListener(HandleCharacterSelectedSignal);
            EventSelectedSignal.RemoveListener(HandleEventSelectedSignal);
            PointOfInterestSelectedSignal.RemoveListener(HandlePointOfInterestSelectedSignal);
            EntityDeselectedSignal.RemoveListener(HandleEntityDeselectedSignal);
            EntityRemovedFromNewSignal.RemoveListener(HandleEntityRemovedFromNewSignal);

            EntityUnlockedSignal.RemoveListener(HandleEntityUnlockedSignal);
            EntityLockedSignal.RemoveListener(HandleEntityLockedSignal);

            GlossaryStartedSignal.RemoveListener(HandleGlossaryStartedSignal);

            View.BackButtonClicked -= OnBackButtonClicked;
            View.AddButtonClicked -= OnAddButtonClicked;
            View.RemoveButtonClicked -= OnRemoveButtonClicked;
            View.EntityButtonClicked -= OnEntityButtonClicked;
        }

        void HandleGlossaryStartedSignal()
        {
            View.CreateEntityEntries(
                EntitySetModel.Entities,
                EntityGroupSetModel.EntityGroups,
                EntitySetModel.EntityParents,
                EntitySetModel.EntityChildren,
                EntitySetModel.EntityTags,
                EntitySetModel.EntityLocks
            );

            // View.SelectEntityEntry(Model.SelectedEntityID);
            SelectEntitySignal.Dispatch(EntitySetModel.SelectedEntityID);

            foreach (var entryKVP in LocationSetModel.Locations)
                View.DisplayLocationEntry(entryKVP.Key, entryKVP.Value.Name);

            foreach (var entryKVP in CharacterSetModel.Characters)
                View.DisplayCharacterEntry(entryKVP.Key, entryKVP.Value.Name, entryKVP.Value.ImagePath);

            foreach (var entryKVP in EventSetModel.Events)
                View.DisplayEventEntry(entryKVP.Key, entryKVP.Value.Name);

            foreach (var entryKVP in PointOfInterestSetModel.PointsOfInterest)
                View.DisplayPointOfInterestEntry(entryKVP.Key, entryKVP.Value.Name);
        }

        private void HandleEntitySelectedSignal(int entityID)
        {
            View.SelectEntityEntry(entityID);

            View.SetImage(EntitySetModel.Entities[entityID].ImagePath);
        }

        private void HandleEntityUnlockedSignal(int entityID)
        {
            View.AddEntityEntry(
                entityID,
                EntitySetModel.Entities[entityID],
                EntityGroupSetModel.EntityGroups,
                EntitySetModel.EntityParents[entityID],
                EntitySetModel.EntityChildren[entityID],
                EntitySetModel.EntityTags[entityID],
                EntitySetModel.EntityLocks[entityID]
            );

            if (LocationSetModel.Locations.ContainsKey(entityID))
                View.DisplayLocationEntry(
                    entityID,
                    LocationSetModel.Locations[entityID].Name
                );

            if (CharacterSetModel.Characters.ContainsKey(entityID))
                View.DisplayCharacterEntry(
                    entityID,
                    CharacterSetModel.Characters[entityID].Name,
                    CharacterSetModel.Characters[entityID].ImagePath
                );

            if (EventSetModel.Events.ContainsKey(entityID))
                View.DisplayEventEntry(
                    entityID,
                    EventSetModel.Events[entityID].Name
                );

            if (PointOfInterestSetModel.PointsOfInterest.ContainsKey(entityID))
                View.DisplayPointOfInterestEntry(
                    entityID,
                    PointOfInterestSetModel.PointsOfInterest[entityID].Name
                );
        }

        private void HandleEntityLockedSignal(int entityID)
        {
            View.RemoveEntityEntry(
                entityID
            );
        }

        private void HandleLocationSelectedSignal(int entityID)
        {
            View.ToggleCollapseEntityEntry(entityID);

            View.DisplayEntityDetails(
                LocationSetModel.Locations[entityID].Name,
                LocationSetModel.Locations[entityID].Description
            );
        }

        private void HandleCharacterSelectedSignal(int entityID)
        {
            View.DisplayEntityDetails(
                CharacterSetModel.Characters[entityID].Name,
                CharacterSetModel.Characters[entityID].Description
            );
        }

        private void HandleEventSelectedSignal(int entityID)
        {
            View.DisplayEntityDetails(
                EventSetModel.Events[entityID].Name,
                EventSetModel.Events[entityID].Description
            );
        }

        private void HandlePointOfInterestSelectedSignal(int entityID)
        {
            View.DisplayEntityDetails(
                PointOfInterestSetModel.PointsOfInterest[entityID].Name,
                PointOfInterestSetModel.PointsOfInterest[entityID].Description
            );
        }

        private void HandleEntityDeselectedSignal(int entityID)
        {
            View.DeselectEntityEntry(entityID);
        }

        private void HandleEntityRemovedFromNewSignal(int entityID)
        {
            View.MarkEntityEntryOld(entityID);
        }

        private void OnBackButtonClicked()
        {
            SelectScreenSignal.Dispatch(ScreenFSMCommand.Back);
        }

        private void OnEntityButtonClicked(int entityID)
        {
            SelectEntitySignal.Dispatch(entityID);
        }

        private void OnAddButtonClicked()
        {
            UnlockRandomEntitySignal.Dispatch();
        }

        private void OnRemoveButtonClicked()
        {
            LockRandomEntitySignal.Dispatch();
        }
    }
}