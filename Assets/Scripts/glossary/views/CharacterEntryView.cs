﻿using System;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;

namespace com.bitvillain.glossary
{
    public class CharacterEntryView : View    
    {
        [SerializeField]
        Text titleText;

        [SerializeField]
        Image portraitImage;

        public void SetTitleText(string text)
        {
            if(titleText != null)
                titleText.text = text;
        }

        internal void SetImage(string imagePath)
        {
            if(portraitImage != null)
                portraitImage.sprite = Resources.Load<Sprite>(imagePath);
        }
    }
}