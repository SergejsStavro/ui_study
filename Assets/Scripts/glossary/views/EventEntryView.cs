﻿using System;
using UnityEngine;
using UnityEngine.UI;
using strange.extensions.mediation.impl;

namespace com.bitvillain.glossary
{
    public class EventEntryView : View    
    {
        [SerializeField]
        Text titleText;

        public void SetTitleText(string text)
        {
            if(titleText != null)
                titleText.text = text;
        }
    }
}