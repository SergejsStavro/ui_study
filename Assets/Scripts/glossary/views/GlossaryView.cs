﻿using UnityEngine;
using strange.extensions.mediation.impl;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using com.bitvillain.listScroll;
using com.bitvillain.tutorial;
using com.bitvillain.entity;

namespace com.bitvillain.glossary
{
    public class GlossaryView : View
    {
        [SerializeField]
        Button backButton;

        [SerializeField]
        Image entityImage;

        [SerializeField]
        Button addButton;

        [SerializeField]
        Button removeButton;

        [SerializeField]
        Button toggleSubcategoriesButton;

        public Action BackButtonClicked;

        public Action AddButtonClicked;

        public Action RemoveButtonClicked;

        [SerializeField]
        ListScrollView listScrollView;

        [SerializeField]
        DetailScrollView detailScrollView;


        [SerializeField]
        GameObject locationCategoryPrefab;


        [SerializeField]
        GameObject characterSubcategoryPrefab;

        [SerializeField]
        GameObject eventSubcategoryPrefab;

        [SerializeField]
        GameObject pointOfInterestSubcategoryPrefab;


        [SerializeField]
        GameObject characterEntryPrefab;

        [SerializeField]
        GameObject eventEntryPrefab;

        [SerializeField]
        GameObject pointOfInterestEntryPrefab;


        Dictionary<EntityType, GameObject> entryPrefabs;
        Dictionary<EntityType, GameObject> subcategoryPrefabs;


        Dictionary<int, EntityGroupEntryView> entityGroupEntryViews;
        Dictionary<int, LocationEntryView> locationEntryViews;
        Dictionary<int, CharacterEntryView> characterEntryViews;
        Dictionary<int, EventEntryView> eventEntryViews;
        Dictionary<int, PointOfInterestEntryView> pointOfInterestEntryViews;

        public event Action<int> EntityButtonClicked
        {
            add
            {
                listScrollView.EntryButtonClicked += value;
            }
            remove
            {
                listScrollView.EntryButtonClicked -= value;
            }

        }

        bool subcategoriesVisible = true;


        override protected void OnEnable()
        {
            base.OnEnable();
            backButton.onClick.AddListener(OnBackButtonClicked);
            addButton.onClick.AddListener(OnAddButtonClicked);
            removeButton.onClick.AddListener(OnRemoveButtonClicked);
            toggleSubcategoriesButton.onClick.AddListener(OnToggleSubcategoriesButtonClicked);

            //TODO refactor
            entryPrefabs = new Dictionary<EntityType, GameObject>()
            {
                {EntityType.Location, locationCategoryPrefab},
                {EntityType.Character, characterEntryPrefab},
                {EntityType.Event, eventEntryPrefab},
                {EntityType.PointOfInterest, pointOfInterestEntryPrefab},
            };

            subcategoryPrefabs = new Dictionary<EntityType, GameObject>()
            {
                {EntityType.Character, characterSubcategoryPrefab},
                {EntityType.Event, eventSubcategoryPrefab},
                {EntityType.PointOfInterest, pointOfInterestSubcategoryPrefab},
            };
        }

        override protected void OnDisable()
        {
            base.OnDisable();
            backButton.onClick.RemoveAllListeners();
            addButton.onClick.RemoveAllListeners();
            removeButton.onClick.RemoveAllListeners();
            toggleSubcategoriesButton.onClick.RemoveAllListeners();
        }

        private void OnBackButtonClicked()
        {
            if (BackButtonClicked != null)
                BackButtonClicked();
        }

        private void OnAddButtonClicked()
        {
            if (AddButtonClicked != null)
                AddButtonClicked();
        }

        private void OnRemoveButtonClicked()
        {
            if (RemoveButtonClicked != null)
                RemoveButtonClicked();
        }

        private void OnToggleSubcategoriesButtonClicked()
        {
            subcategoriesVisible = !subcategoriesVisible;
            listScrollView.SetAllBranchVisibility(subcategoriesVisible);
        }

        internal void SetImage(string imagePath)
        {
            if (entityImage != null)
            {
                entityImage.enabled = imagePath != string.Empty;
                entityImage.sprite = Resources.Load<Sprite>(imagePath);
            }
        }

        internal void CreateEntityEntries(
            Dictionary<int, EntityModel> entityModels,
            Dictionary<int, EntityGroupModel> entityGroupModels,
            Dictionary<int, EntityParentModel> entityParents,
            Dictionary<int, EntityChildrenModel> entityChildren,
            Dictionary<int, EntityTagModel> entityTags,
            Dictionary<int, EntityLockModel> entityLocks
        )
        {
            entityGroupEntryViews = new Dictionary<int, EntityGroupEntryView>();
            locationEntryViews = new Dictionary<int, LocationEntryView>();
            characterEntryViews = new Dictionary<int, CharacterEntryView>();
            eventEntryViews = new Dictionary<int, EventEntryView>();
            pointOfInterestEntryViews = new Dictionary<int, PointOfInterestEntryView>();

            List<ListItemModel> listRecords =
                new List<ListItemModel>();

            int entityID;
            ListItemModel listRecord;
            foreach (var entityModelKVP in entityModels)
            {
                entityID = entityModelKVP.Value.ID;

                listRecord = CreateEntityEntry(
                    entityID,
                    entityModelKVP.Value,
                    entityGroupModels,
                    entityParents[entityID],
                    entityChildren[entityID],
                    entityTags[entityID],
                    entityLocks[entityID]
                );

                if (listRecord != null)
                    listRecords.Add(listRecord);
            }

            listScrollView.AddEntries(listRecords);
        }

        internal ListItemModel CreateEntityEntry(
            int entityID,
            EntityModel entityModel,
            Dictionary<int, EntityGroupModel> entityGroupModels,
            EntityParentModel entityParent,
            EntityChildrenModel entityChildren,
            EntityTagModel entityTags,
            EntityLockModel entityLock
        )
        {
            GameObject entryGameObject;
            ListItemModel listRecord = null;

            if (!entityLock.IsLocked)
            {
                if (entityModel.Type != EntityType.EntityGroup)
                {
                    entryGameObject = Instantiate<GameObject>(entryPrefabs[entityModel.Type]);

                    LoadLocationEntry(entityID, entryGameObject);
                    LoadCharacterEntry(entityID, entryGameObject);
                    LoadEventEntry(entityID, entryGameObject);
                    LoadPointOfInterestEntry(entityID, entryGameObject);
                }
                else
                {
                    EntityType type = entityGroupModels[entityID].Type;
                    entryGameObject = Instantiate<GameObject>(subcategoryPrefabs[type]);
                    LoadEntityGroupEntry(entityID, entryGameObject);
                }

                listRecord = new ListItemModel(
                    entityID,
                    entryGameObject,
                    entityTags.Tags,
                    entityParent.ParentEntity,
                    entityChildren.ChildEntities,
                    entityLock.UnlockTimestamp,
                    entityLock.IsNew
                );

            }

            return listRecord;
        }

        internal void RemoveEntityEntry(int entityID)
        {
            if (entityGroupEntryViews.ContainsKey(entityID))
                entityGroupEntryViews.Remove(entityID);

            if (locationEntryViews.ContainsKey(entityID))
                locationEntryViews.Remove(entityID);

            if (characterEntryViews.ContainsKey(entityID))
                characterEntryViews.Remove(entityID);

            if (eventEntryViews.ContainsKey(entityID))
                eventEntryViews.Remove(entityID);

            if (pointOfInterestEntryViews.ContainsKey(entityID))
                pointOfInterestEntryViews.Remove(entityID);

            listScrollView.RemoveEntry(entityID);
        }

        internal void AddEntityEntry(
            int entityID,
            EntityModel entityModel,
            Dictionary<int, EntityGroupModel> entityGroupModels,
            EntityParentModel entityParent,
            EntityChildrenModel entityChildren,
            EntityTagModel entityTags,
            EntityLockModel entityLock
        )
        {
            var listRecord = CreateEntityEntry(
                entityID,
                entityModel,
                entityGroupModels,
                entityParent,
                entityChildren,
                entityTags,
                entityLock
            );

            listScrollView.AddEntry(listRecord);
            listScrollView.SortEntries();
        }

        internal void MarkEntityEntryOld(int entityID)
        {
            listScrollView.MarkEntryOld(entityID);
        }

        void LoadEntityGroupEntry(int entryID, GameObject entry)
        {
            var entryView = entry.GetComponent<EntityGroupEntryView>();
            if (entryView != null)
                entityGroupEntryViews.Add(entryID, entryView);
        }

        void LoadLocationEntry(int entryID, GameObject entry)
        {
            var entryView = entry.GetComponent<LocationEntryView>();
            if (entryView != null)
                locationEntryViews.Add(entryID, entryView);
        }

        void LoadCharacterEntry(int entryID, GameObject entry)
        {
            var entryView = entry.GetComponent<CharacterEntryView>();
            if (entryView != null)
                characterEntryViews.Add(entryID, entryView);
        }

        void LoadEventEntry(int entryID, GameObject entry)
        {
            var entryView = entry.GetComponent<EventEntryView>();
            if (entryView != null)
                eventEntryViews.Add(entryID, entryView);
        }

        void LoadPointOfInterestEntry(int entryID, GameObject entry)
        {
            var entryView = entry.GetComponent<PointOfInterestEntryView>();
            if (entryView != null)
                pointOfInterestEntryViews.Add(entryID, entryView);
        }


        internal void DisplayLocationEntry(int entityID, string name)
        {
            if (locationEntryViews.ContainsKey(entityID))
                locationEntryViews[entityID].SetTitleText(name);
        }

        internal void DisplayCharacterEntry(int entityID, string name, string imagePath)
        {
            if (characterEntryViews.ContainsKey(entityID))
            {
                characterEntryViews[entityID].SetTitleText(name);
                characterEntryViews[entityID].SetImage(imagePath);
            }
        }


        internal void DisplayEventEntry(int entityID, string name)
        {
            if (eventEntryViews.ContainsKey(entityID))
                eventEntryViews[entityID].SetTitleText(name);
        }


        internal void DisplayPointOfInterestEntry(int entityID, string name)
        {
            if (pointOfInterestEntryViews.ContainsKey(entityID))
                pointOfInterestEntryViews[entityID].SetTitleText(name);
        }

        internal void SelectEntityEntry(int entityID)
        {
            listScrollView.SelectEntry(entityID);
        }

        internal void DisplayEntityDetails(string name, string description)
        {
            detailScrollView.SetHeaderText(name);
            detailScrollView.SetBodyText(description);
        }

        internal void DeselectEntityEntry(int entityID)
        {
            listScrollView.DeselectEntry(entityID);
        }

        internal void ToggleCollapseEntityEntry(int entityID)
        {
            listScrollView.ToggleCollapseEntry(entityID);
        }
    }
}