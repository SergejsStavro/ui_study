#FOLDERS AND FRAMEWORKS
To start the project open the Main scene which is located in bitVillain/Main/Scenes

Scenes folder contains the task scenes. (Inventory scene is a placeholder)
Scripts folder contains all the relevant code. 

bitVillain folder contains my private framework, it's a work in progress.

All the code in com.bitvillain.* namespace is written by me.

Additional framework used: StrangeIoC it enforces the MVCS architecture and allows the use of dependency injection in a convenient way. 

#IMPLEMENTATION DETAILS
The idea behind the way the code is organized is similar to entity-component-system pattern. Each list item is an entity with additional components specific to each screen that add functionality on top of the base list item functionality. This allows the list to be flexible and reusable in many different scenarios.

Each list item stores a list of it's child ids and it's parent id. this data is used to organize the list items in a desired configuration. The list item game object hierarchy is always flat.

For the search feature each list item stores a list of tags that are checked against the search terms. the first element of the tag list is also used for alphabetic sorting.

For newest to oldest sort an additional timestamp is stored.
