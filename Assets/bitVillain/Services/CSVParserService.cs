﻿using System.Collections.Generic;
using System.Text;

namespace com.bitvillain.services
{
    //TODO large files will be a problem
    //TODO comas and newlines in quotes not supported
    public class CSVParserService
    {
        public List<string[]> GetRowsFromString(string data, bool ignoreFirst = false)
        {
            data = data.Replace("\r\n", "\n").Replace('\r', '\n');

            var rowsRaw = data.Split('\n');

            var rows = new List<string[]>();

            int startingRowIndex = ignoreFirst ? 1 : 0;

            for (var i = startingRowIndex; i < rowsRaw.Length; i++) // skip first line, it's a header
            {
                var columns = rowsRaw[i].Split(',');
                //TODO can use split only if all the cells are not quoted

                if (columns.Length > 1)
                    rows.Add(columns);
            }

            return rows;
        }

        internal string GetStringFromRows(List<string[]> rows)
        {
            StringBuilder builder = new StringBuilder();
            foreach (var row in rows)
                builder.Append(string.Join(",", row)).Append('\n');

            return builder.ToString();
        }
    }
}