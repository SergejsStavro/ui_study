﻿namespace com.bitvillain.services
{
    public interface IFileLoaderService
    {
        string LoadString(string filePath);
        byte[] LoadByteArray(string filePath);
    }
}