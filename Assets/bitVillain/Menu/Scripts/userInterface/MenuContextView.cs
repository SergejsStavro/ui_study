﻿using strange.extensions.context.impl;

namespace com.bitvillain.ui
{
    public class MenuContextView : ContextView
    {
        void Awake()
        {
            context = new MenuContext(this);
            context.Start();
        }

        override protected void OnDestroy()
        {
            base.OnDestroy();
        }
    }   
}
