﻿using strange.extensions.mediation.impl;
using UnityEngine;

namespace com.bitvillain.ui
{
    public class MenuMediator : Mediator
    {
        [Inject]
        public MenuView view{ get; set;}
        
        [Inject]
        public SelectScreenSignal SelectScreenSignal { get; private set; }

        public override void OnRegister()
        {
            view.ScreenButtonClicked += OnScreenButtonClicked;
        }
        
        public override void OnRemove()
        {
            view.ScreenButtonClicked -= OnScreenButtonClicked;
        }

        private void OnScreenButtonClicked(ScreenFSMCommand screenFSMCommand)
        {
            SelectScreenSignal.Dispatch(screenFSMCommand);
        }
    }
}