﻿using System.Collections.Generic;
using com.bitvillain.fsm;
using com.bitvillain.services;
using UnityEngine;
using ScreenFSMTransition = com.bitvillain.fsm.FiniteStateMachineTransition<ScreenFSMState, ScreenFSMCommand>;

namespace com.bitvillain.ui
{   
    public class UserInterfaceModel
    {
        #region data loading
        public const string DATA_PATH = "USER_INTERFACE_MODEL_DATA_PATH";

        [Inject(DATA_PATH)]
        public string DataPath { get; private set; }
        [Inject]
        public CSVParserService CSVParserService { get; private set; }
        [Inject]
        public ResourceFileLoaderService ResourceFileLoaderService { get; private set; }
        #endregion

        public FiniteStateMachine<ScreenFSMState, ScreenFSMTransition, ScreenFSMCommand> ScreenFSM =
                new FiniteStateMachine<ScreenFSMState, ScreenFSMTransition, ScreenFSMCommand>();

        public void InitializeFSM()
        {
            var transitions = CSVParserService.GetRowsFromString(
                ResourceFileLoaderService.LoadString(DataPath)
            );

            InitializeFSM(transitions);
        }

        public void InitializeFSM(List<string[]> transitions)
        {
            int columnCount = 0;
            for (int i = 0; i < transitions.Count; i++)
            {
                columnCount = 0;
                for (int k = 0; k < transitions[i].Length; k++)
                    if(transitions[i][k] != string.Empty)
                        columnCount++;

                switch (columnCount)
                {
                    case 4:
                        ScreenFSM.AddUndirectedTransition(
                            Utils.GetEnumElementFromName<ScreenFSMState>(transitions[i][0]),
                            Utils.GetEnumElementFromName<ScreenFSMCommand>(transitions[i][1]),
                            Utils.GetEnumElementFromName<ScreenFSMState>(transitions[i][2]),
                            Utils.GetEnumElementFromName<ScreenFSMCommand>(transitions[i][3])
                        );
                        break;
                    case 3:
                        ScreenFSM.AddDirectedTransition(
                            Utils.GetEnumElementFromName<ScreenFSMState>(transitions[i][0]),
                            Utils.GetEnumElementFromName<ScreenFSMCommand>(transitions[i][1]),
                            Utils.GetEnumElementFromName<ScreenFSMState>(transitions[i][2])
                        );
                        break;
                    case 1:
                        ScreenFSM.SetInitialState(
                            Utils.GetEnumElementFromName<ScreenFSMState>(transitions[i][0])
                        );
                        break;
                    default:
                    break;
                }
            }
        }
    }
}