﻿using UnityEngine;
using strange.extensions.command.impl;
using strange.extensions.context.api;

namespace com.bitvillain.ui
{
    public class StartUserInterfaceCommand : Command
    {
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject contextView { get; set; }

        [Inject]
        public UserInterfaceModel UserInterfaceModel { get; private set; }

        [Inject]
        public ScreenSelectedSignal ScreenSelectedSignal { get; private set; }
        
        public override void Execute()
        {
            UserInterfaceModel.InitializeFSM();
            UserInterfaceModel.ScreenFSM.StateEntered += OnScreenFSMStateEntered;
            UserInterfaceModel.ScreenFSM.Initialize();
        }

        private void OnScreenFSMStateEntered(ScreenFSMState state)
        {
            ScreenSelectedSignal.Dispatch(state);
        }

    }
}