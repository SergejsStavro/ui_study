﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using UnityEngine.SceneManagement;

namespace com.bitvillain.ui
{
    public class SwitchScenesCommand : Command
    {
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject contextView { get; set; }
        [Inject]
        public ScreenFSMState ScreenFSMState { get; private set; }

        public override void Execute()
        {
            contextView.GetComponent<MonoBehaviour>().StartCoroutine(ShowLoadingAsync());
        }

        public IEnumerator ShowLoadingAsync()
        {
            yield return SceneManager.LoadSceneAsync(ScreenFSMState.ToString());
        }
    }
}