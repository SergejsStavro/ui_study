using strange.extensions.command.impl;

namespace com.bitvillain.ui
{
    public class SelectScreenCommand : Command
    {
        [Inject]
        public UserInterfaceModel Model { get; private set; }

        [Inject]
        public ScreenFSMCommand ScreenFSMCommand { get; private set; }

        [Inject]
        public ScreenSelectedSignal ScreenSelectedSignal { get; private set; }
        public override void Execute()
        {
            Model.ScreenFSM.MakeTransition(ScreenFSMCommand);

            // ScreenSelectedSignal.Dispatch(Model.ScreenFSM.State);
        }
    }
}