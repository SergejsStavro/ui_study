﻿using UnityEngine;
using System.Collections;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using UnityEngine.SceneManagement;

public class HideLoadingCommand : Command
{
	[Inject(ContextKeys.CONTEXT_VIEW)]
	public GameObject contextView{get;set;}
	
	public override void Execute()
	{
		contextView.GetComponent<MonoBehaviour>().StartCoroutine(HideLoadingAsync());
	}
	
	public IEnumerator HideLoadingAsync()
	{
		yield return SceneManager.UnloadSceneAsync("LoadingScreen");
		//TODO fade in
	}
}
