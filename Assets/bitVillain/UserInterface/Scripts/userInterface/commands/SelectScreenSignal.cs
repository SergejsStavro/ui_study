﻿using strange.extensions.signal.impl;

namespace com.bitvillain.ui
{
    public class SelectScreenSignal : Signal<ScreenFSMCommand> { }
}