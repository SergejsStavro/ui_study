﻿public enum ScreenFSMCommand
{
    None = 0,
    ToMenu = 1,
    ToGlossary = 2,
    ToInventory = 3,
    ToTutorial = 4,
    
    Back = 1000,
}
