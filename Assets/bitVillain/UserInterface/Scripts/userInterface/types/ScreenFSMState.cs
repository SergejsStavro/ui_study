﻿public enum ScreenFSMState
{
    None = 0,
    Menu = 1,
    Glossary = 2,
    Inventory = 3,
    Tutorial = 4,
}