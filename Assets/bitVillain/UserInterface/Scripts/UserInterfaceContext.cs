﻿using com.bitvillain.services;
using UnityEngine;

namespace com.bitvillain.ui
{
    public class UserInterfaceContext : SignalContext
    {
        public UserInterfaceContext(MonoBehaviour view) : base(view)
        {

        }

        protected override void mapBindings()
        {
            base.mapBindings();

            #region services
            injectionBinder
                .Bind<CSVParserService>()
                .ToSingleton();
            injectionBinder
                .Bind<ResourceFileLoaderService>()
                .ToSingleton();
            #endregion

            #region values
            injectionBinder
                .Bind<string>()
                .ToValue("data/UserInterfaceStates")
                .ToName(UserInterfaceModel.DATA_PATH);
            #endregion

            #region models
            injectionBinder
                .Bind<UserInterfaceModel>()
                .ToSingleton().CrossContext();
            #endregion

            #region signals
            injectionBinder
                .Bind<SelectScreenSignal>()
                .ToSingleton().CrossContext();

            injectionBinder
                .Bind<ScreenSelectedSignal>()
                .ToSingleton().CrossContext();
            #endregion

            #region HUD
            //TODO separate context for hud?

            // mediationBinder.Bind<HUDPanelView>().To<HUDPanelMediator>();

            #endregion


            #region commands
            commandBinder
                .Bind<SelectScreenSignal>()
                .To<SelectScreenCommand>();

            commandBinder
                .Bind<ScreenSelectedSignal>()
                // .InSequence()
                // .To<ShowLoadingCommand>()
                .To<SwitchScenesCommand>();// .To<HideLoadingCommand>()

            //always last
            commandBinder
                .Bind<StartContextSignal>()
                .InSequence()
                .To<ShowLoadingCommand>()
                .To<StartUserInterfaceCommand>()
                .To<HideLoadingCommand>()
                .Once();
            #endregion
        }

        public override void AddView(object view)
        {
            Debug.Log("AddView " + view.ToString());
            base.AddView(view);
        }

        public override void RemoveView(object view)
        {
            Debug.Log("RemoveView " + view.ToString());
            base.RemoveView(view);
        }
    }
}