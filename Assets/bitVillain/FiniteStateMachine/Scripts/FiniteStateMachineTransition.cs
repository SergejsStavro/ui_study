﻿using System.Collections.Generic;

namespace com.bitvillain.fsm
{
    public class FiniteStateMachineTransition<S, C> : IFiniteStateMachineTransition<S, C>
    {
        S _state;
        C _command;

        public S State
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;
            }
        }

        public C Command
        {
            get
            {
                return _command;
            }
            set
            {
                _command = value;
            }
        }

        public FiniteStateMachineTransition() { }

        public FiniteStateMachineTransition(S state, C command)
        {
            _state = state;
            _command = command;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            if (_state == null || _command == null)
            {
                hash = base.GetHashCode();
            }
            else
            {
                hash = hash * 31 + _state.GetHashCode();
                hash = hash * 31 + _command.GetHashCode();
            }

            return hash;
        }

        public override bool Equals(object otherTransitionObject)
        {
            var otherTransition =
                otherTransitionObject as FiniteStateMachineTransition<S, C>;

            return
                _state != null &&
                _command != null &&
                otherTransition != null &&
                EqualityComparer<S>.Default.Equals(_state, otherTransition._state) &&
                EqualityComparer<C>.Default.Equals(_command, otherTransition._command);
        }
    }
}