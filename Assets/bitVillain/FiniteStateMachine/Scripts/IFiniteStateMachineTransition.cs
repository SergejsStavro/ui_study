﻿namespace com.bitvillain.fsm
{
    public interface IFiniteStateMachineTransition<S, C>
    {
        S State { set; }
        C Command { set; }
    }
}