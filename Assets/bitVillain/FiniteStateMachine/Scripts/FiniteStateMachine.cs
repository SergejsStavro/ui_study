﻿using System;
using System.Collections.Generic;

namespace com.bitvillain.fsm
{
    public class FiniteStateMachine<S, T, C> : IFiniteStateMachine<S, T, C>
        where T : IFiniteStateMachineTransition<S, C>, new()
    {
        protected Dictionary<T, S> Transitions = new Dictionary<T, S>();
        protected S InitialState;
        public S State { get; protected set; }
        public event Action<S> StateExited;
        public event Action<S> StateEntered;

        public FiniteStateMachine() { }

        public void Initialize()
        {
            State = InitialState;

            if (StateEntered != null)
                StateEntered(State);
        }

        public IFiniteStateMachine<S, T, C> SetInitialState(S state)
        {
            InitialState = state;
            return this;
        }

        public IFiniteStateMachine<S, T, C> MakeTransition(C command)
        {
            var transition = new T {State = State, Command = command};

            S nextState;
            if (Transitions.TryGetValue(transition, out nextState))
            {
                if (StateExited != null)
                    StateExited(State);

                State = nextState;

                if (StateEntered != null)
                    StateEntered(State);
            }
            return this;
        }

        public IFiniteStateMachine<S, T, C> AddDirectedTransition(S stateA, C commandAB, S stateB)
        {
            var transition = new T {State = stateA, Command = commandAB};

            Transitions.Add(transition, stateB);
            return this;
        }

        public IFiniteStateMachine<S, T, C> AddUndirectedTransition(S stateA, C commandAB, S stateB, C commandBA)
        {
            AddDirectedTransition(stateA, commandAB, stateB);
            AddDirectedTransition(stateB, commandBA, stateA);
            return this;
        }

        public IFiniteStateMachine<S, T, C> AddSelfTransition(S state, C command)
        {
            AddDirectedTransition(state, command, state);
            return this;
        }

        public bool IsCommandValid(C command)
        {
            var transition = new T {State = State, Command = command};

            return Transitions.ContainsKey(transition);
        }
    }
}
