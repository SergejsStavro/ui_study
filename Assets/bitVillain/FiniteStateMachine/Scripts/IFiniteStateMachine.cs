﻿namespace com.bitvillain.fsm
{
    public interface IFiniteStateMachine<S, T, C>
    {
        S State { get; }
        void Initialize();
        IFiniteStateMachine<S, T, C> SetInitialState(S state);
        IFiniteStateMachine<S, T, C> MakeTransition(C command);
        IFiniteStateMachine<S, T, C> AddDirectedTransition(S stateA, C commandAB, S stateB);
        IFiniteStateMachine<S, T, C> AddUndirectedTransition(S stateA, C commandAB, S stateB, C commandBA);
        IFiniteStateMachine<S, T, C> AddSelfTransition(S state, C command);
        bool IsCommandValid(C command);
    }
}