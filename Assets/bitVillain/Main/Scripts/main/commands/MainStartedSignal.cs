﻿using strange.extensions.signal.impl;

namespace com.bitvillain.main
{
    public class MainStartedSignal : Signal { }
}