﻿using strange.extensions.context.impl;

namespace com.bitvillain.main
{
    public class MainContextView : ContextView
    {
        void Awake()
        {
            context = new MainContext(this);
            context.Start();
        }
    }
}