﻿using UnityEngine;
using com.bitvillain.services;

namespace com.bitvillain.main
{
    public class MainContext : SignalContext
    {
        public MainContext(MonoBehaviour view) : base(view) { }

        protected override void mapBindings()
        {
            base.mapBindings();

            #region services
            injectionBinder
                .Bind<CSVParserService>()
                .ToSingleton().CrossContext();
            injectionBinder
                .Bind<ResourceFileLoaderService>()
                .ToSingleton().CrossContext();
            #endregion

            commandBinder
                .Bind<StartContextSignal>()
                .To<StartMainCommand>()
                .Once();
        }
    }
}