﻿using com.bitvillain.services;
using UnityEngine;

namespace com.bitvillain.sound
{
    public class SoundContext : SignalContext
    {
        public SoundContext(MonoBehaviour view) : base(view)
        {

        }

        protected override void mapBindings()
        {
            base.mapBindings();

            #region services
            injectionBinder
                .Bind<CSVParserService>()
                .ToSingleton();
            injectionBinder
                .Bind<ResourceFileLoaderService>()
                .ToSingleton();
            #endregion

            #region values
            injectionBinder
                .Bind<string>()
                .ToValue("data/SoundPaths")
                .ToName(SoundModel.DATA_PATH);
            #endregion

            #region models
            injectionBinder
                .Bind<SoundModel>()
                .ToSingleton().CrossContext();
            #endregion

            // #region signals
            injectionBinder
                .Bind<PlaySoundSignal>()
                .ToSingleton().CrossContext();

            // injectionBinder
            //     .Bind<ScreenSelectedSignal>()
            //     .ToSingleton().CrossContext();
            // #endregion

            mediationBinder.Bind<SoundView>().To<SoundMediator>();


            #region commands
            commandBinder
                .Bind<PlaySoundSignal>()
                .To<PlaySoundCommand>();

            // commandBinder
            //     .Bind<ScreenSelectedSignal>()
            //     // .InSequence()
            //     // .To<ShowLoadingCommand>()
            //     .To<SwitchScenesCommand>();// .To<HideLoadingCommand>()

            //always last
            commandBinder
                .Bind<StartContextSignal>()
                // .InSequence()
                // .To<ShowLoadingCommand>()
                .To<StartSoundContextCommand>()
                // .To<HideLoadingCommand>()
                .Once();
            #endregion
        }

        public override void AddView(object view)
        {
            Debug.Log("AddView " + view.ToString());
            base.AddView(view);
        }

        public override void RemoveView(object view)
        {
            Debug.Log("RemoveView " + view.ToString());
            base.RemoveView(view);
        }
    }
}