﻿using strange.extensions.context.impl;

namespace com.bitvillain.sound
{
    public class SoundContextView : ContextView
    {
        void Awake()
        {
            context = new SoundContext(this);
            context.Start();
        }
    }
}