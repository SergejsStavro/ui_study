﻿using UnityEngine;
using strange.extensions.mediation.impl;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

namespace com.bitvillain.sound
{
    public class SoundView : View
    {
        [SerializeField]
        Button[] _buttons;

        [SerializeField]
        List<SoundType> _soundTypes;

        public Action<SoundType> ButtonClicked;

        override protected void OnEnable()
        {
            base.OnEnable();
            for (int i = 0; i < _buttons.Length; i++)
            {
                var k = i;
                if(k < _soundTypes.Count)
                    _buttons[i].onClick.AddListener(() => OnScreenButtonClicked(_soundTypes[k]) );
            }
        }

        override protected void OnDisable()
        {
            base.OnDisable();
            for (int i = 0; i < _buttons.Length; i++)
                _buttons[i].onClick.RemoveAllListeners();
        }

        private void OnScreenButtonClicked(SoundType SoundType)
        {
            if(ButtonClicked != null)
                ButtonClicked(SoundType);
        }
    }
}