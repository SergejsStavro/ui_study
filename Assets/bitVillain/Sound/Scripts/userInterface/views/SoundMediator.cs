﻿using strange.extensions.mediation.impl;
using UnityEngine;

namespace com.bitvillain.sound
{
    public class SoundMediator : Mediator
    {
        [Inject]
        public SoundView View{ get; set;}
        
        [Inject]
        public PlaySoundSignal PlaySoundSignal { get; private set; }

        public override void OnRegister()
        {
            View.ButtonClicked += OnButtonClicked;
        }
        
        public override void OnRemove()
        {
            View.ButtonClicked -= OnButtonClicked;
        }

        private void OnButtonClicked(SoundType soundType)
        {
            // Debug.Log("SoundMediator::OnButtonClicked " + soundType);
            PlaySoundSignal.Dispatch(soundType);
        }
    }
}