﻿using System;
using System.Collections.Generic;
using com.bitvillain.services;
using UnityEngine;

namespace com.bitvillain.sound
{
    public class SoundModel
    {
        #region data loading
        public const string DATA_PATH = "SOUND_MODEL_DATA_PATH";

        [Inject(DATA_PATH)]
        public string DataPath { get; private set; }
        [Inject]
        public CSVParserService CSVParserService { get; private set; }
        [Inject]
        public ResourceFileLoaderService ResourceFileLoaderService { get; private set; }
        #endregion

        enum Column
        {
            Type = 0,
            Path = 1
        }

        public Dictionary<SoundType, string> SoundPaths = new Dictionary<SoundType, string>();

        internal string GetSoundPath(SoundType soundType)
        {
            if (!SoundPaths.ContainsKey(soundType))
                Debug.Log("Doesn't contain " + soundType);

            return SoundPaths.ContainsKey(soundType) ? SoundPaths[soundType] : "";
        }

        public void Initialize()
        {
            var sounds = CSVParserService.GetRowsFromString(
                ResourceFileLoaderService.LoadString(DataPath),
                true
            );

            ParseSoundPaths(sounds);
        }

        public void ParseSoundPaths(List<string[]> soundEntries)
        {
            for (int i = 0; i < soundEntries.Count; i++)
            {
                if (soundEntries[i].Length > 1 &&
                    soundEntries[i][(int)Column.Type] != string.Empty)
                {
                    SoundPaths.Add(
                        Utils.GetEnumElementFromName<SoundType>(
                            soundEntries[i][(int)Column.Type].Replace(" ", "")
                        ),
                        soundEntries[i][(int)Column.Path]
                    );
                }
            }
        }
    }
}