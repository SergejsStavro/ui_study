﻿public enum SoundType
{
    None = 0,
    DefaultButtonSound = 1,
    CloseButtonSound = 2,
    Game = 3,
    Over = 4,
}