using strange.extensions.command.impl;
using UnityEngine;

namespace com.bitvillain.sound
{
    public class PlaySoundCommand : Command
    {
        [Inject]
        public SoundModel SoundModel { get; private set; }

        [Inject]
        public SoundType SoundType { get; private set; }

        public override void Execute()
        {
            //TODO cache frequently used sounds
            var audioClip = Resources.Load<AudioClip>(SoundModel.GetSoundPath(SoundType));
            AudioSource.PlayClipAtPoint(audioClip, Camera.main.transform.position);
        }
    }
}