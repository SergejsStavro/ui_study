﻿using strange.extensions.signal.impl;

namespace com.bitvillain.sound
{
    public class PlaySoundSignal : Signal<SoundType> { }
}