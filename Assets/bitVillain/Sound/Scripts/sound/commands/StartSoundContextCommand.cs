﻿using UnityEngine;
using strange.extensions.command.impl;
using strange.extensions.context.api;

namespace com.bitvillain.sound
{
    public class StartSoundContextCommand : Command
    {
        [Inject(ContextKeys.CONTEXT_VIEW)]
        public GameObject contextView { get; set; }

        [Inject]
        public SoundModel SoundModel { get; private set; }


        public override void Execute()
        {
            SoundModel.Initialize();
        }
    }
}